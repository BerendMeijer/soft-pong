struct win32_window_thread_parameters
{
    IN HINSTANCE ProcessInstance; //< Needed for the creation of the window.
    IN int ShowCode; //< Needed for the creation of the window.
    
    OUT HWND WindowHandle; //< A reference to the windowhandle created by the window thread.
    OUT HDC DeviceContext; //< A reference to the devicecontext of the window created by the window thread.
    
    OUT bool32 WindowIsReady; //< Is true when the window is initialized. 
    OUT bool32 WindowCreationError; //< Is true whenever an error occured during the initialization of the windows window.
    OUT bool32 IsRunning; //< Is set to false when the escape key is pressed.
};
