/// Gets the minimum value from a list of floating point values.
/// @param  a_List The list of values to search in.
/// @param a_Count The end of the range to search in:
/// so the search range is from element 0 to and including a_Count.
// TODO(Berend): Make this work with std::vector.
internal real32
GetMinimumFromVertexList(real32 a_List[CONVEX_MAX_VERTEX_COUNT], uint16 a_Count)
{
    // Initialize with the maximum for this value because 0 could be a valid value.
    real32 Result = 0xFFFFFF;
    
    Assert(a_Count <= CONVEX_MAX_VERTEX_COUNT);
    
    for(uint16 Index = 0; Index < a_Count; ++Index)
    {
        if(a_List[Index] < Result)
        {
            Result = a_List[Index];
        }
    }
    return Result;
}

/// Gets the maximum value from a list of floating point values.
/// @param  a_List The list of values to search in.
/// @param a_Count The end of the range to search in:
/// so the search range is from element 0 to and including a_Count.
internal real32
GetMaximumFromVertexList(real32 a_List[CONVEX_MAX_VERTEX_COUNT], uint16 a_Count)
{
    // Initialize to the minimum for this value because 0 
    // could be a valid value.
    real32 Result = -0xFFFFFF;
    
    Assert(a_Count <= CONVEX_MAX_VERTEX_COUNT);
    
    for(uint16 Index = 0; Index < a_Count; ++Index)
    {
        if(a_List[Index] > Result)
        {
            Result = a_List[Index];
        }
    }
    return Result;
}

