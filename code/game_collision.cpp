 /// Checks if two convex shapes are overlapping on an axis.
 /// It does so by projecting all vertices of the shapes onto the axis
 /// and checking the overlap. 
 /// It returns the minimum overlap in a_MinimumOverlap and the collision normal in a_CollisionNormal.
 /// Returns true if there was an overlap on the axis.
internal bool32
     Check1DOverlap(IN game_vec2 a_AxisToCheck, 
                    IN const game_vertex_list& a_ConvexA, 
                    IN const game_vertex_list& a_ConvexB, 
                    OUT real32& a_MinimumOverlap, 
                    OUT game_vec2& a_CollisionNormal)
 {
     bool32 IsOverlapping = true;
     
     real32 AxisProjectionListA[CONVEX_MAX_VERTEX_COUNT] = {};
     real32 AxisProjectionListB[CONVEX_MAX_VERTEX_COUNT] = {};
     
     //------------- Project points ---------------------
     // Project all of convex A's points.
     for(uint16 DotIndex = 0; DotIndex < a_ConvexA.VertexCount; ++DotIndex)
     {
         AxisProjectionListA[DotIndex] = DotProduct(a_AxisToCheck, a_ConvexA.Vertices[DotIndex]); 
     }
     // Project all of convex B's points.
     for(uint16 DotIndex = 0; DotIndex < a_ConvexB.VertexCount; ++DotIndex)
     {
         // Project points.
         AxisProjectionListB[DotIndex] = DotProduct(a_AxisToCheck, a_ConvexB.Vertices[DotIndex]); 
     }
     real32 ConvexAMinimum = GetMinimumFromVertexList(AxisProjectionListA, a_ConvexA.VertexCount);
     real32 ConvexAMaximum = GetMaximumFromVertexList(AxisProjectionListA, a_ConvexA.VertexCount);
     
     real32 ConvexBMinimum = GetMinimumFromVertexList(AxisProjectionListB, a_ConvexB.VertexCount);
     real32 ConvexBMaximum = GetMaximumFromVertexList(AxisProjectionListB, a_ConvexB.VertexCount);
     
     // Shape A overlaps B on the left side on this axis.
     bool32 AOverlapsBLeftSide = (ConvexAMinimum < ConvexBMinimum && 
                                  ConvexAMaximum > ConvexBMinimum && 
                                  ConvexAMaximum < ConvexBMaximum); 
     
     // Shape A overlaps B on the right side on this axis.
     bool32 AOverlapsBRightSide = (ConvexAMinimum > ConvexBMinimum && 
                                   ConvexAMinimum < ConvexBMaximum && 
                                   ConvexAMaximum > ConvexBMaximum);
     
     // Shape A is surrounded by shape B on this axis.
     bool32 AIsInsideB = (ConvexBMinimum < ConvexAMinimum && 
                          ConvexBMinimum < ConvexAMaximum && 
                          ConvexBMaximum > ConvexAMinimum && 
                          ConvexBMaximum > ConvexAMaximum);
     
     // Shape B is surrounded by shape A on this axis.
     bool32 BIsInsideA = (ConvexAMinimum < ConvexBMinimum && 
                          ConvexAMinimum < ConvexBMaximum && 
                          ConvexAMaximum > ConvexBMinimum && 
                          ConvexAMaximum > ConvexBMaximum);
     
     
     if(AOverlapsBLeftSide || AOverlapsBRightSide|| AIsInsideB ||  BIsInsideA)
     {
         real32 Overlap = 0.0f; 
         if(AOverlapsBLeftSide)// Shape A overlaps B on the left side. 
         {
             Overlap = ConvexAMaximum - ConvexBMinimum;
             // Only set the when the overlaps are not the same.
         }
         else if(AOverlapsBRightSide)// Shape A overlaps B on the right side.
         {
             Overlap = ConvexBMaximum - ConvexAMinimum;
             
             a_AxisToCheck.X *= -1;
             a_AxisToCheck.Y *= -1;
         }
         else if(AIsInsideB) // Shape A is inside shape B.
         {
             // Calculate the smallest overlap here and choose the smallest one.
             real32 OverlapLeft =  ConvexAMaximum - ConvexBMinimum;
             real32 OverlapRight = ConvexBMaximum - ConvexAMinimum;
             
             if(OverlapLeft < OverlapRight)
             {
                 Overlap = OverlapLeft;
             }
             else
             {
                 Overlap = OverlapRight;
                 
                 // Flip the collision normal that is pointing to the left from A by default. 
                 a_AxisToCheck.X *= -1;
                 a_AxisToCheck.Y *= -1;
             }
         }
         else if(BIsInsideA) // Shape B is inside shape A.
         {
             // Calculate the smallest overlap here and choose the smallest one.
             real32 OverlapLeft = ConvexBMaximum - ConvexAMinimum;
             real32 OverlapRight = ConvexAMaximum - ConvexBMinimum;
             if(OverlapLeft < OverlapRight)
             {
                 Overlap = OverlapLeft;
                 
                 // Flip the collision normal because now B is inside A, the collision normal turns around. 
                 a_AxisToCheck.X *= -1;
                 a_AxisToCheck.Y *= -1;
             }
             else
             {
                 Overlap = OverlapRight;
             }
         }
         
         // At this point a nonzero overlap value should have been set.
         Assert(Overlap != 0.0f);
         if(Overlap < a_MinimumOverlap)
         {
             a_MinimumOverlap = Overlap;
             a_CollisionNormal = a_AxisToCheck;
         }
     }
     else
     {
         IsOverlapping = false;
     }
     return(IsOverlapping);
 }
 
 
 /// Checks if two convex shapes are overlapping and returns the collision normal and overlap relative to a_ConvexA.
 /// If you would like to set the shape outside of each other, move a_ConvexB in the direction of the collision normal times the overlap. 
 /// Returns true when there is an overlap.
 /// OUT values are only valid when the function returns true.
 /// It uses the Seperating Axis Theorem: http://www.dyn4j.org/2010/01/sat/
 internal bool32
     ConvexCheckCollision(IN const game_vertex_list& a_ConvexA,
                          IN const game_vertex_list& a_ConvexB,
                          OUT real32& a_MinimumOverlap,
                          OUT game_vec2& a_CollisionNormal)
 {
     // Initialize the result to true. If any non-overlapping axis is found, it is set to false
     // and there is no collision. 
     bool32 IsOverlapping = true;
     // Initialize to max value because this will hold the minimum.
     real32 Overlap = 0xFFFFFF;
     
     // This is the axis that is pointing from one vertex to the next in the vertexlist of the convex collider.
     // It should always be pointing left-to-right because we requested users to fill the list in clockwise order.
     game_vec2 AxisToCheck = {};
     
     // Check all axes except for the one between the last and first vertex in the list.
     for(uint16 Index = 0; Index < a_ConvexA.VertexCount - 1; ++Index)
     {
         // Only continue checking if all the previous axes found an overlap.
         if(IsOverlapping)
         {
             AxisToCheck= GetNormalizedAxis(a_ConvexA.Vertices[Index], a_ConvexA.Vertices[Index + 1]);  
             AxisToCheck= GetPerpendicularVectorCounterClockwise(AxisToCheck);
             
             IsOverlapping = Check1DOverlap(AxisToCheck, a_ConvexA, a_ConvexB, Overlap, a_CollisionNormal);
         }
         else
         {
             // Stop checking: the last check resulted in a non-overlapping axis.
             // This means that there cannot be a collision.
             // There can only be a collision if there was an overlap on every axes.
             break;
         }
     }
     
     // At this point all axes between the vertices are checked except for the first and last one.
     // So that is what we do here if all the previous axes were found to be overlapping.
     if(IsOverlapping)
     {
         AxisToCheck= GetNormalizedAxis(a_ConvexA.Vertices[(a_ConvexA.VertexCount - 1)], a_ConvexA.Vertices[0]);
         AxisToCheck= GetPerpendicularVectorCounterClockwise(AxisToCheck);
         IsOverlapping = Check1DOverlap(AxisToCheck, a_ConvexA, a_ConvexB, Overlap, a_CollisionNormal);
     }
     
     a_MinimumOverlap = Overlap;
     return(IsOverlapping);
 }
 
 /// Updates the position of the vertices, using the vertices offsets.
 internal void
     UpdateCollider(game_object* a_GameObject)
 {
     game_convex_collider2D* Collider = GetComponent<game_convex_collider2D>(*a_GameObject);
     if(Collider)
     {
         // Update the position of all vertices.
         for(uint16 Index = 0; Index < Collider->VertexList.VertexCount; ++Index)
         {
             Collider->VertexList.Vertices[Index].X = a_GameObject->Position.X + Collider->VertexList.Offsets[Index].X; 
             Collider->VertexList.Vertices[Index].Y = a_GameObject->Position.Y + Collider->VertexList.Offsets[Index].Y;
         }
     }
 }
 
 /// Resolves any collisions between gameobjects that have a game_convex_collider2D component and a game_rigidbody2D component.
 /// It resolves interpenetration, and reflects the velocities along the collision normal.
 internal void
     ResolveCollisions(IN std::vector<game_object*>& a_GameObjects)
 {
     for(uint16 IteratorA = 0; IteratorA < a_GameObjects.size(); ++IteratorA)
     {
         // Only check the game object against other colliders if it has a collider.
         game_convex_collider2D* ColliderA = GetComponent<game_convex_collider2D>(*a_GameObjects[IteratorA]);
         
         if(ColliderA)
         {
             for(uint16 IteratorB = 0; IteratorB < a_GameObjects.size(); ++IteratorB)
             {
                 game_convex_collider2D* ColliderB = GetComponent<game_convex_collider2D>(*a_GameObjects[IteratorB]);
                 // Dont check colliders against themselves.
                 if(ColliderB && (ColliderA != ColliderB))
                 {
                     game_rigidbody2D* RigidBodyA = GetComponent<game_rigidbody2D>(*a_GameObjects[IteratorA]);
                     game_rigidbody2D* RigidBodyB = GetComponent<game_rigidbody2D>(*a_GameObjects[IteratorB]);
                     
                     // Both objects need to have a rigid body component for the collision to make sense (for now).
                     // TODO(Berend): Dont make this a hard requirement.
                     if(RigidBodyA && RigidBodyB)
                     {
                         game_vec2 CollisionNormalRelativeToA = {};
                         game_vec2 CollisionNormalRelativeToB = {};
                         real32 OverlapA = 0; //< How much does object A overlap?
                         real32 OverlapB = 0; //< How much does object B overlap?
                         
                         // We need to check two times, relative to each object to determine if there was a collision.
                         if(ConvexCheckCollision(ColliderB->VertexList, ColliderA->VertexList, OverlapA, CollisionNormalRelativeToB))
                         {
                             if(ConvexCheckCollision(ColliderA->VertexList, ColliderB->VertexList, OverlapB, CollisionNormalRelativeToA))
                             {
                                 //-------- Resolve interpenetration ---------
                                 game_vec2 PenetrationCorrection = {};
                                 // Choose the rigidbody that is not kinematic, if there is one.
                                 if(!RigidBodyB->IsKinematic)
                                 {
                                     // Resolve the penetration, and scale the peneration correction vector up by a little bit
                                     // to make sure that they are really not touching anymore.
                                     PenetrationCorrection.X = (OverlapB * CollisionNormalRelativeToA.X) * 1.0001f;
                                     PenetrationCorrection.Y = (OverlapB * CollisionNormalRelativeToA.Y) * 1.0001f;
                                     
                                     a_GameObjects[IteratorB]->Position.X += PenetrationCorrection.X;
                                     a_GameObjects[IteratorB]->Position.Y += PenetrationCorrection.Y;
                                     
                                     // Update the collider position for the next round of collision checks.
                                     UpdateCollider(a_GameObjects[IteratorB]);
                                 }
                                 else if(!RigidBodyA->IsKinematic)
                                 {
                                     // Resolve the penetration, and scale the peneration correction vector up by a little bit
                                     // to make sure that they are really not touching anymore.
                                     PenetrationCorrection.X = (OverlapA * CollisionNormalRelativeToB.X) * 1.0001f;
                                     PenetrationCorrection.Y = (OverlapA * CollisionNormalRelativeToB.Y) * 1.0001f;
                                     
                                     // The collision normal is flipped when the other object is kinematic.
                                     a_GameObjects[IteratorA]->Position.X += (PenetrationCorrection.X);
                                     a_GameObjects[IteratorA]->Position.Y += (PenetrationCorrection.Y);
                                     // Update the collider position for the next round of collision checks.
                                     UpdateCollider(a_GameObjects[IteratorA]);
                                 }
                                 
                                 //------- Set new velocities --------------
                                 // TODO(Berend): Implement elastic collision and realistic rigidbody dynamics using mass.
                                 // For now: reflect the vectors of both rigidbodies.
                                 RigidBodyA->Velocity = ReflectVector(RigidBodyA->Velocity, CollisionNormalRelativeToB);
                                 RigidBodyB->Velocity = ReflectVector(RigidBodyB->Velocity, CollisionNormalRelativeToA);
                                 
                                 // Play impact sound.
                                 game_synth_envelope Env = {};
                                 PlayNote(e_waveform::SINE, 100, 30.0, 0.0f, Env, 1.0f);
                                 PlayNote(e_waveform::SQUARE, 200, 10.0, 0.0f, Env, 0.2f);
                             }
                         }
                     }
                 }
             }
         }
     }
}