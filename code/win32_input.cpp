// Initializes XInput
internal void 
Win32InitXInput()
{
    HMODULE XInput = 0;
    // Try to load the latest version of XInput. 
    XInput = LoadLibrary("XInput1_4.dll");
    if(!XInput)
    {
        XInput = LoadLibrary("XInput1_3.dll");
    }
    
    //If the XInput DLL was loaded, we can point to the original functions again.
    if(XInput)
    {
        void* GetStateFunctionAddress = GetProcAddress(XInput, "XInputGetState");
        if(GetStateFunctionAddress)
        {
            XInputGetState_ = (decltype(XInputGetState_))GetStateFunctionAddress;
        }
        
        void* SetStateFunctionAddress = GetProcAddress(XInput, "XInputSetState");
        if(SetStateFunctionAddress)
        {
            XInputSetState_ = (decltype(XInputSetState_))SetStateFunctionAddress;
        }
    }
}

/// Processes the state of any joystick button. 
internal game_button_state
Win32ProcessXInputButton(uint16 a_ButtonBitmask, WORD a_Buttons, game_button_state a_PreviousButtonState)
{
    game_button_state CurrentButtonState = {};
    // Gets a nonzero value when it is true.
    CurrentButtonState.IsDown = (a_Buttons & a_ButtonBitmask) ? 1 : 0;
    CurrentButtonState.WasDown = (a_PreviousButtonState.IsDown && !CurrentButtonState.IsDown);
    
    // A button cant ever have these two flags set to true at the same time.
    Assert(!(CurrentButtonState.IsDown && CurrentButtonState.WasDown));
    
    // Increment halftransitioncounts if there were changes in buttonstates.
    if(a_PreviousButtonState.IsDown && !CurrentButtonState.IsDown)
    {
        CurrentButtonState.HalfTransitionCount++;
    }
    else if(!a_PreviousButtonState.IsDown && CurrentButtonState.IsDown)
    {
        CurrentButtonState.HalfTransitionCount++;
    }
    return CurrentButtonState;
}

/// Processes the state of any joystick thumbstick.
internal game_thumbstick_state 
Win32ProcessXInputThumbstick(const real32 a_StickValue, uint16 a_DeadzoneTreshold, game_thumbstick_state a_PreviousThumbStickState) 
{
    game_thumbstick_state ThumbStickState = {};
    
    if((fabs(a_StickValue)) > a_DeadzoneTreshold)
    {
        // Dividing by the limits to get a limit of -1.0f to 1.0f.
        // Dividing by different values here because of uneven limits.
        ThumbStickState.Value = a_StickValue > 0 ? a_StickValue/ 32767.0f :a_StickValue/ 32768.0f;
    }
    
    ThumbStickState.Velocity = ThumbStickState.Value - a_PreviousThumbStickState.Value;
    return ThumbStickState;
}

/// Updates the state of all joysticks that are connected.
internal void 
Win32ProcessJoyStickInput(IN OUT win32_joystick_states& a_JoystickStates)
{
    XINPUT_STATE ControllerState = {};
    // The first joystick slot is reserved for the keyboard.
    // So the first joystick will write to the 1st element of game_joystick_input.
    for(DWORD i = 1; i < XUSER_MAX_COUNT + 1; ++i)
    {
        if(XInputGetState(i - 1, &ControllerState) == ERROR_SUCCESS)
        {
            WORD Buttons = ControllerState.Gamepad.wButtons;
            // A reference to the new joystick state that we're calculating for this joystick.
            game_joystick_state &NewJoystickState = a_JoystickStates.NewState[i];
            // A reference to the previous joystick state for this joystick.
            game_joystick_state const &PreviousJoystickState = a_JoystickStates.PreviousState[i];
            
            // ABXY.
            NewJoystickState.Buttons.AButton = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_A, Buttons, PreviousJoystickState.Buttons.AButton);
            
            NewJoystickState.Buttons.BButton = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_B, Buttons, PreviousJoystickState.Buttons.BButton);
            
            NewJoystickState.Buttons.YButton = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_Y, Buttons, PreviousJoystickState.Buttons.YButton);
            
            NewJoystickState.Buttons.XButton = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_X, Buttons, PreviousJoystickState.Buttons.XButton);
            
            //Start and back.
            NewJoystickState.Buttons.StartButton = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_START, Buttons, PreviousJoystickState.Buttons.StartButton);
            
            NewJoystickState.Buttons.BackButton = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_BACK, Buttons, PreviousJoystickState.Buttons.BackButton);
            
            //Dpad.
            NewJoystickState.Buttons.PadUp = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_DPAD_UP, Buttons, PreviousJoystickState.Buttons.PadUp);
            
            NewJoystickState.Buttons.PadDown = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_DPAD_DOWN, Buttons, PreviousJoystickState.Buttons.PadDown);
            
            NewJoystickState.Buttons.PadRight = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_DPAD_RIGHT, Buttons, PreviousJoystickState.Buttons.PadRight);
            
            NewJoystickState.Buttons.PadLeft = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_DPAD_LEFT, Buttons, PreviousJoystickState.Buttons.PadLeft);
            
            // Shoulder buttons.
            NewJoystickState.Buttons.LeftShoulder = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_LEFT_SHOULDER, Buttons, PreviousJoystickState.Buttons.LeftShoulder);
            
            NewJoystickState.Buttons.RightShoulder = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_RIGHT_SHOULDER, Buttons, PreviousJoystickState.Buttons.RightShoulder);
            
            // Thumb stick presses.
            NewJoystickState.Buttons.LeftThumb = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_LEFT_THUMB, Buttons, PreviousJoystickState.Buttons.LeftThumb);
            
            NewJoystickState.Buttons.RightThumb = 
                Win32ProcessXInputButton(XINPUT_GAMEPAD_RIGHT_THUMB, Buttons, PreviousJoystickState.Buttons.RightThumb);
            
            // Thumbsticks.
            NewJoystickState.LeftStickX = 
                Win32ProcessXInputThumbstick(ControllerState.Gamepad.sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE, PreviousJoystickState.LeftStickX);
            
            NewJoystickState.LeftStickY = 
                Win32ProcessXInputThumbstick(ControllerState.Gamepad.sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE, PreviousJoystickState.LeftStickY);
            
            NewJoystickState.RightStickX = 
                Win32ProcessXInputThumbstick(ControllerState.Gamepad.sThumbRX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE, PreviousJoystickState.RightStickX);
            
            NewJoystickState.RightStickY = 
                Win32ProcessXInputThumbstick(ControllerState.Gamepad.sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE, PreviousJoystickState.RightStickY);
            
            // Triggers.
            if(ControllerState.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
            {
                NewJoystickState.RightTrigger.Value = ControllerState.Gamepad.bRightTrigger/ 255.0f;
            }
            NewJoystickState.RightTrigger.Velocity = NewJoystickState.RightTrigger.Value - PreviousJoystickState.RightTrigger.Value;
            
            
            if(ControllerState.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
            {
                NewJoystickState.LeftTrigger.Value = ControllerState.Gamepad.bLeftTrigger/ 255.0f;
            }
            NewJoystickState.LeftTrigger.Velocity = NewJoystickState.LeftTrigger.Value - PreviousJoystickState.LeftTrigger.Value;
        }
        
        else
        {
            //OutputDebugStringA("Controller not connected.");
        }
    }
}


/// Processes a keyboard button as if it is a joystick button.
internal game_button_state
Win32ProcessKeyboardButton(SHORT a_KeyState, const game_button_state& a_PreviousButtonState)
{
    game_button_state NewButtonState = {};
    
    // Gets a nonzero value when it is true.
    NewButtonState.IsDown = (a_KeyState) ? 1 : 0;
    NewButtonState.WasDown = (a_PreviousButtonState.IsDown && !NewButtonState.IsDown);
    
    // A button cant ever have these two flags set to true at the same time.
    Assert(!(NewButtonState.IsDown && NewButtonState.WasDown));
    
    // Increment halftransitioncounts if there were changes in buttonstates.
    if(a_PreviousButtonState.IsDown && !NewButtonState.IsDown)
    {
        NewButtonState.HalfTransitionCount++;
    }
    else if(!a_PreviousButtonState.IsDown && NewButtonState.IsDown)
    {
        NewButtonState.HalfTransitionCount++;
    }
    return NewButtonState;
};

/// Updates the state of the keyboard controller.  
/// The joystick at element [0] is the keyboard.
internal void
Win32ProcessKeyboardInput(IN OUT win32_joystick_states& a_JoystickStates)
{
    // A reference to the new joystick state that we're getting for this joystick.
    // The keyboard "joystick" is always mapped to the first element. 
    game_joystick_state &NewJoyStickState = a_JoystickStates.NewState[0];
    // A reference to the previous joystick state for this joystick.
    game_joystick_state const &PreviousJoyStickState = a_JoystickStates.PreviousState[0];
    
    NewJoyStickState.Buttons.PadUp = Win32ProcessKeyboardButton(GetAsyncKeyState(VK_UP), PreviousJoyStickState.Buttons.PadUp);
    NewJoyStickState.Buttons.PadDown = Win32ProcessKeyboardButton(GetAsyncKeyState(VK_DOWN), PreviousJoyStickState.Buttons.PadDown);
    NewJoyStickState.Buttons.PadLeft = Win32ProcessKeyboardButton(GetAsyncKeyState(VK_LEFT), PreviousJoyStickState.Buttons.PadLeft);
    NewJoyStickState.Buttons.PadRight = Win32ProcessKeyboardButton(GetAsyncKeyState(VK_RIGHT), PreviousJoyStickState.Buttons.PadRight);
    
    //'W'
    NewJoyStickState.Buttons.AButton = Win32ProcessKeyboardButton(GetAsyncKeyState(0x57), PreviousJoyStickState.Buttons.AButton);
    //'S'
    NewJoyStickState.Buttons.BButton = Win32ProcessKeyboardButton(GetAsyncKeyState(0x53), PreviousJoyStickState.Buttons.BButton);
    
    //====== Is any button down? =====
    // Point to the first button in the struct.
    game_button_state* Button = &NewJoyStickState.Buttons.AButton;
    // Calculate the total amount of buttons.
    // We currently support 14 buttons (all buttons on a Xbox360 joystick).
    uint16 ButtonCount = sizeof(NewJoyStickState.Buttons) / sizeof(game_button_state);
    // Loop over all buttons to check if any of them are down.
    for(uint16 ButtonIndex = 0; ButtonIndex < ButtonCount; ++ButtonIndex)
    {
        if(Button->IsDown)
    {
        NewJoyStickState.AnyButtonDown = true;
    }
    Button++;
}
    }
