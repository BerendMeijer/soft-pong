
/// Initializes WASAPI and returns pointers to the interfaces 
/// in the arguments. Returns null pointers if the function failed.
/// Returns true if the initialization succeeded.
internal bool32
Win32InitWASAPI(IN  win32_sound_output a_SoundOutSettings,
OUT IAudioClient** a_AudioClient,  
                OUT IAudioRenderClient** a_AudioRenderClient)
{
    bool32 Result = 0;
    *a_AudioClient = 0;
    *a_AudioRenderClient = 0;
    
    if(SUCCEEDED(CoInitialize(0)))
    {
        IMMDeviceEnumerator* MultiMediaDeviceEnum = 0;
        
        if(SUCCEEDED(CoCreateInstance(__uuidof(MMDeviceEnumerator), 
                                      0,
                                      CLSCTX_ALL,
                                      __uuidof(IMMDeviceEnumerator),
                                      (void**)&MultiMediaDeviceEnum)))
        {
            // Get the default 'audio end point' for rendering(writing) a stream of audio.
            IMMDevice* MultiMediaDevice = 0;
            if(SUCCEEDED(MultiMediaDeviceEnum->GetDefaultAudioEndpoint(eRender,
                                                                       eConsole,
                                                                       &MultiMediaDevice)))
            {
                // We need to call activate before we can initialize.
                IAudioClient* AudioClient = 0;
                if(SUCCEEDED(MultiMediaDevice->Activate(__uuidof(IAudioClient),
                                                        CLSCTX_ALL,
                                                        0, (void**)&AudioClient)))
                {
                    // A 1 second buffer. 
                    REFERENCE_TIME BufferDuration = 10000000; 
                    
                    WAVEFORMATEX RequestedWaveFormat = {};
                    RequestedWaveFormat.wFormatTag = WAVE_FORMAT_PCM;
                    RequestedWaveFormat.nChannels = 2;
                    RequestedWaveFormat.nSamplesPerSec = a_SoundOutSettings.SamplesPerSecond;
                    RequestedWaveFormat.wBitsPerSample = a_SoundOutSettings.BitsPerSample;
                    // nBlockAlign: How many bytes are there in one LEFT RIGHT block.
                    RequestedWaveFormat.nBlockAlign = (uint16)((RequestedWaveFormat.nChannels * RequestedWaveFormat.wBitsPerSample) / 8);
                    RequestedWaveFormat.nAvgBytesPerSec = RequestedWaveFormat.nSamplesPerSec * RequestedWaveFormat.nBlockAlign;
                    
                    // In the future, we need to play nice and ask which samplerate we need to be running in 
                    // instead of demanding a samplerate.
                    
                    WAVEFORMATEX* ClosestWaveFormat = 0;
                    
                    HRESULT InitResult = 0;
                    if(AudioClient->IsFormatSupported(AUDCLNT_SHAREMODE_SHARED,
                                                                     &RequestedWaveFormat,
                                                      &ClosestWaveFormat) == S_OK)
                    {
                        // If the requested format is supported, initialize the audioclient with that.
                        InitResult = AudioClient->Initialize(AUDCLNT_SHAREMODE_SHARED, 0,
                                                BufferDuration, 0,
                                                &RequestedWaveFormat, 0);
                    } // TODO(Berend): Add else if result is S_FALSE, we can try running in the closest wave format.
                    else
                    {
                        // Else try to run in exclusive mode: this should be regarded as a last resort option.
                        // In my test, it bugged out other applications that were using the audio device
                        // in another format.
                        InitResult = AudioClient->Initialize(AUDCLNT_SHAREMODE_EXCLUSIVE, 0,
                                                BufferDuration, 0,
                                                &RequestedWaveFormat, 0);
                    }
                    
                    if(SUCCEEDED(InitResult))
                    {
                    // Check if we got the number of samples that we requested.
                    uint32 BufferSamples = 0;
                    AudioClient->GetBufferSize(&BufferSamples);
                    Assert(BufferSamples > 0);
                    
                    // Get a pointer to an audio render client so we can write samples to the audio endpoint.
                    IAudioRenderClient* AudioRenderClient = 0;
                    if(SUCCEEDED (AudioClient->GetService(__uuidof(IAudioRenderClient),(void**)&AudioRenderClient)))
                    {
                        Result = true;
                        *a_AudioRenderClient = AudioRenderClient;
                        *a_AudioClient = AudioClient;
                    }
                }
            }
            }
        }
    }
    return Result;
}


/// Copies the gamesound output into directsound memory.
internal void 
Win32CopySoundBuffer(IN IAudioRenderClient* a_AudioRenderClient, 
                     IN game_sound_out* a_GameSoundOutput)
{   
    if(a_AudioRenderClient)
    {
    BYTE* DataPointer = 0;
    
    HRESULT Result = a_AudioRenderClient->GetBuffer(a_GameSoundOutput->SampleCount, &DataPointer);
    
            // Point to the start of the game output buffer. 
            int16* SampleSource = (int16*) a_GameSoundOutput->BufferMemory;
            // Point to the start of the first region.
            int16* SampleDest = (int16*)DataPointer;
            
            //Copy into first region.
    for(uint32 SampleIndex = 0; SampleIndex < a_GameSoundOutput->SampleCount; ++SampleIndex)
            {
                *SampleDest++ = *SampleSource++;
                *SampleDest++ = *SampleSource++;
            }
            
            // Say how many samples we've written, and give 0 as second argument when 
            // we haven't only written silence. Else: AUDCLNT_BUFFERFLAGS_SILENT
            Result = a_AudioRenderClient->ReleaseBuffer(a_GameSoundOutput->SampleCount, 0);
}
}
