/// - The platform layer supplies the game with a block of memory 
/// to render into using this struct. 
struct game_bitmap
{
    void* Memory; ///< A pointer to the start of the the bitmap memory.
    uint32 Width; ///< The bitmap width in pixels.
    uint32 Height; ///< The bitmap height in pixels.
    uint16 BytesPerPixel; ///< The amount of bytes needed to store one pixel.
};

/// Holds a color ranging from 0 - 255 for each color.
struct game_color
{
    uint8 Red;
    uint8 Green;
    uint8 Blue;
};
     