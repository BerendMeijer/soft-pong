/// Contains the controller states for all controllers.
/// We keep two states so we can check for button transitions.
struct win32_joystick_states
{
    game_joystick_state PreviousState[5];
    game_joystick_state NewState[5];
};
