     /// Holds 2 floating point values that should be used together.
         struct game_vec2
     {
         real32 X;
         real32 Y;
     };
     
     inline
     game_vec2 operator+(IN game_vec2 a_VectorA, IN game_vec2 a_VectorB)
     {
         game_vec2 Result = {};
         
         Result.X = a_VectorA.X + a_VectorB.X;
         Result.Y = a_VectorA.Y + a_VectorB.Y;
         
         return Result;
     }
     
     inline
         game_vec2& operator+=(IN game_vec2& a_VectorA, IN game_vec2 a_VectorB)
     {
         a_VectorA = a_VectorA + a_VectorB;
         return a_VectorA;
     }
     
     
     inline
         game_vec2 operator-(IN game_vec2 a_VectorA, IN game_vec2 a_VectorB)
     {
         game_vec2 Result = {};
         
         Result.X = a_VectorA.X - a_VectorB.X;
         Result.Y = a_VectorA.Y - a_VectorB.Y;
         
         return Result;
     }
     
     
     inline
         game_vec2& operator-=(IN game_vec2& a_VectorA, IN game_vec2 a_VectorB)
     {
         a_VectorA = a_VectorA - a_VectorB;
         return a_VectorA;
     }
     
     
