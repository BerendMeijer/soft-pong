struct game_button_state
{
    bool32 IsDown;
    bool32 WasDown;
    uint16 HalfTransitionCount; //< The amount of times the button went from Up to Down and Down to Up in one timeslot.
};

struct game_thumbstick_state
{
    real32 Value;
    real32 Velocity;
};

struct game_joystick_trigger_state
{
    real32 Value;
    real32 Velocity;
};

struct game_buttons
{
    // Dont change the order of this! It maps directly to the synth keyboard.
    game_button_state AButton;
    game_button_state BButton;
    game_button_state XButton;
    game_button_state YButton;
    game_button_state PadLeft;
    game_button_state PadRight;
    game_button_state PadUp;
    game_button_state PadDown;
    game_button_state StartButton;
    game_button_state BackButton;
    game_button_state LeftShoulder;
    game_button_state RightShoulder;
    game_button_state LeftThumb;
    game_button_state RightThumb;
};

/// - Every platform has to fill out this joystick state structure to
/// provide the game with input.
/// - Limits for sticks and triggers:  -1.0f and 1.0f.
/// - Buttons are true when down.
struct game_joystick_state
{
    game_thumbstick_state LeftStickX;
    game_thumbstick_state LeftStickY;
    game_thumbstick_state RightStickX;
    game_thumbstick_state RightStickY;
    
    game_joystick_trigger_state LeftTrigger;
    game_joystick_trigger_state RightTrigger;
    
    bool32 AnyButtonDown;
    game_buttons Buttons;
};

struct game_input
{
    game_joystick_state Joysticks[5]; //< 1 Keyboard and 4 Gamepads.
    
    bool32 (*PlatformLoadFile)(IN char* a_FileName, IN bool32 a_IsBMP, IN uint32 a_FileSize, OUT void* a_FileMemory, OUT game_bitmap& ImageInfo); 
    //void* PlaformWriteFile;
    uint32 (*PlatformGetFileSize)(IN char* a_FileName);
    
    real64 DeltaTime; //< The amount of ms to progress over this frame. 
    uint16 GameUpdateHz;
};
