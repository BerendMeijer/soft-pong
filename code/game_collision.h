     /// Holds all the vertices of a convex collider in clockwise order in world space
     /// so we can do collision checking on them. For now a convex can have
     /// a max of 256 vertices.
     struct game_vertex_list
     {
         game_vec2 Vertices[CONVEX_MAX_VERTEX_COUNT]; //< The actual world position of each vertex.
         game_vec2 Offsets[CONVEX_MAX_VERTEX_COUNT]; //< The offset of each vertex from the origin of the gameobject.
         uint16 VertexCount; //< The amount of vertices in this collider.
     };
     
     struct game_1D_projection_list
     {
         real32 Points[CONVEX_MAX_VERTEX_COUNT];
         uint16 Count;
     };
     