/// Holds sound info that is only relevant to the windows platform.
struct win32_sound_output
{
    uint32 RunningSampleIndex;
    uint16 BitsPerSample;
    
    uint32 SamplesPerSecond;
    uint32 SecondaryBufferSize;
};
