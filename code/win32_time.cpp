/// Gets the time difference in milliseconds, 
/// between a given time and the current time.
internal real64
Win32GetTimeDifference(IN const LARGE_INTEGER a_StartingTime)
{
    LARGE_INTEGER CurrentTime = {};
    LARGE_INTEGER ElapsedMicroseconds {};
    LARGE_INTEGER PerformanceCounterFrequency = {};
    real64 TimeDifference = 0;
    
    QueryPerformanceFrequency(&PerformanceCounterFrequency);
    
    QueryPerformanceCounter(&CurrentTime);
    ElapsedMicroseconds.QuadPart = CurrentTime.QuadPart - a_StartingTime.QuadPart;
    // Convert from ticks to microseconds.
    ElapsedMicroseconds.QuadPart *= 1000000;
    ElapsedMicroseconds.QuadPart /= PerformanceCounterFrequency.QuadPart;
    
    // Divide by 1000 to get miliseconds.
    TimeDifference = (ElapsedMicroseconds.QuadPart / 1000.0f);
    
    return TimeDifference;
}

/// Gets the time difference in milliseconds, 
/// between a given time and the current time.
/// Also returns the current time in a_CurrentTime.
internal real64
Win32GetTimeDifference(IN const LARGE_INTEGER a_StartingTime, 
                       OUT LARGE_INTEGER& a_CurrentTime)
{
    LARGE_INTEGER ElapsedMicroseconds {};
    LARGE_INTEGER PerformanceCounterFrequency = {};
    real64 TimeDifference = 0;
    
    QueryPerformanceFrequency(&PerformanceCounterFrequency);
    
    QueryPerformanceCounter(&a_CurrentTime);
    ElapsedMicroseconds.QuadPart = a_CurrentTime.QuadPart - a_StartingTime.QuadPart;
    // Convert from ticks to microseconds.
    ElapsedMicroseconds.QuadPart *= 1000000;
    ElapsedMicroseconds.QuadPart /= PerformanceCounterFrequency.QuadPart;
    
    // Divide by 1000 to get miliseconds.
    TimeDifference = (ElapsedMicroseconds.QuadPart / 1000.0f);
    
    return TimeDifference;
}
