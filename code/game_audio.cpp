#include <vector>
#include <algorithm>

struct game_scheduled_note
{
    real32 DurationMs; //< The total duration of the note.
    real32 OffsetMs; //< The offset from the start of the frame in miliseconds.
    
    game_osc OscillatorSettings;
};

// Write modes for writing audio buffers.
enum e_WriteMode {Overwrite, Additive};

std::vector<game_scheduled_note> ScheduledNotes;

/// Adds a note to a list of scheduled synthesizer notes. 
/// @param a_WaveForm a_DurationMs The duration of the note in miliseconds. 
/// @param a_WaveForm a_FrameOffsetMs The offset from the next frame boundary in miliseconds.
internal void
PlayNote(IN e_waveform a_WaveForm, IN real32 a_Frequency, 
         IN real32 a_DurationMs, IN real32 a_FrameOffsetMs, 
         IN game_synth_envelope a_Envelope, IN real32 a_Volume) 
{
    game_scheduled_note Note = {};
    
    Note.DurationMs = a_DurationMs;
    Note.OffsetMs = a_FrameOffsetMs;
    
    Note.OscillatorSettings.WaveForm = a_WaveForm;
    Note.OscillatorSettings.Frequency = a_Frequency;
    Note.OscillatorSettings.AmpEnvelope = a_Envelope;
    Note.OscillatorSettings.ToneVolume = a_Volume;
    
    ScheduledNotes.push_back(Note);
}

/// Mixes two signals together using the supplied 
/// mixersetting and outputs them the buffermemory of a_SoundOutput.
/// @param a_Mixer The mixersettings that should be used to mix the two oscillator signals together.
/// @param a_OscillatorMemoryA A pointer to the buffermemory of oscillator A.
/// @param a_OscillatorMemoryB A pointer to the buffermemory of oscillator B.
/// @param a_SoundOutput Contains a pointer to the memory destination to write the mixed signal to
/// as well as the numbers of samples to mix and write from the start of the oscillator memories. 
internal void
MixOscillators(IN game_osc_mixer& a_Mixer, 
               IN void* a_OscillatorMemoryA,
               IN void* a_OscillatorMemoryB,
               OUT game_sound_out& a_SoundOutput)
{
    // Point to the first sample in memory for every buffer.
    int16* SampleOut = (int16*) a_SoundOutput.BufferMemory;
    int16* SampleOscA = (int16*) a_OscillatorMemoryA;
    int16* SampleOscB = (int16*) a_OscillatorMemoryB;
    
    for(uint32 SampleIndex = 0; SampleIndex < a_SoundOutput.SampleCount; ++SampleIndex)
    {
        //Left
        *SampleOut++ = (int16)(((*SampleOscA++ * a_Mixer.VolumeOscA) + (*SampleOscB++ * a_Mixer.VolumeOscB)) / 2);
        //Right
        *SampleOut++ = (int16)(((*SampleOscA++ * a_Mixer.VolumeOscA) + (*SampleOscB++ * a_Mixer.VolumeOscB)) / 2);
    }
}

/// Fills a region of buffer memory with a soundwave specified by a_Oscillator.
/// @param a_SamplesPerSecond This usually is 44100.
/// @param a_SampleCount The number of samples to write from the start of the oscillator memory.
/// @param a_SampleOffset The offset from the start writing from, relative to the start the buffer.
/// @param a_WriteMode Should the buffermemory be overwritten or should the signal be added and averaged?
internal void
OscillatorWriteBuffer(IN uint32 a_SamplesPerSecond,
                   IN uint32 a_SampleCount, 
                      IN uint32 a_SampleOffset,
                      IN e_WriteMode a_WriteMode,
                   IN OUT game_osc& a_Oscillator, 
                      OUT void* a_BufferMemory)  
{
    uint32 nSamplesPerWavePeriod = (uint32)(a_SamplesPerSecond / (real32)a_Oscillator.Frequency);
    // Point to the first sample in the oscillator memory.
    // SampleOffset times two because samples are stored in a block of [LEFT RIGHT] aka "interleaved".
    int16* SampleOut = ((int16*)a_BufferMemory) + (a_SampleOffset * 2);
    
    if(a_Oscillator.WaveForm == e_waveform::SINE)
    {
        // Write a sinewave.
        for(uint32 SampleIndex = 0; SampleIndex < a_SampleCount; ++SampleIndex)
        {
            real32 SineValue =  sinf(a_Oscillator.tSine);
            int16 SampleValue = (int16)(SineValue * (a_Oscillator.ToneVolume * INT16_MAX)); 
            
            if(a_WriteMode == e_WriteMode::Overwrite)
            {
            *SampleOut++ = SampleValue;
            *SampleOut++ = SampleValue;
            }
            else if(a_WriteMode == e_WriteMode::Additive)
            {
                *SampleOut++ = (int16)((*SampleOut + SampleValue) / 2.0f);
                *SampleOut++ = (int16)((*SampleOut + SampleValue) / 2.0f);
            }
            
            a_Oscillator.tSine += 2.0f* PI *1.0f / nSamplesPerWavePeriod;
            
            // Decrement tSine by whole wave periods so it never loses precision. 
            uint32 nWavePeriodsToDecrement = (uint32)(a_Oscillator.tSine / (2.0f * PI));
                a_Oscillator.tSine -= nWavePeriodsToDecrement * (2.0f * PI);
        }
    }
    else if(a_Oscillator.WaveForm == e_waveform::SQUARE)
    {
        // Write a squarewave.
        uint32 HalfSquareWavePeriod = (uint32)(nSamplesPerWavePeriod/2.0f);
        uint32 TotalSampleIndex = a_Oscillator.RunningSampleIndex + a_SampleCount;
        
        while(a_Oscillator.RunningSampleIndex < TotalSampleIndex)
        {
            uint32 Remainder = (a_Oscillator.RunningSampleIndex % nSamplesPerWavePeriod);
            if(Remainder < HalfSquareWavePeriod)
            {
                if(a_WriteMode == e_WriteMode::Overwrite)
                {
                *SampleOut++ = (int16)(a_Oscillator.ToneVolume * INT16_MAX); 
                *SampleOut++ = (int16)(a_Oscillator.ToneVolume * INT16_MAX);
                }
                else if(a_WriteMode == e_WriteMode::Additive)
                {
                    *SampleOut++ = (int16) ((*SampleOut + (a_Oscillator.ToneVolume * INT16_MAX)) / 2.0f); 
                    *SampleOut++ = (int16) ((*SampleOut + (a_Oscillator.ToneVolume * INT16_MAX)) / 2.0f); 
                }
            }
            else
            {
                if(a_WriteMode == e_WriteMode::Overwrite)
                {
                *SampleOut++ = (int16)(-a_Oscillator.ToneVolume * INT16_MAX); 
                *SampleOut++ = (int16)(-a_Oscillator.ToneVolume * INT16_MAX);
                }
                else if(a_WriteMode == e_WriteMode::Additive)
                {
                    *SampleOut++ = (int16) ((*SampleOut + (-a_Oscillator.ToneVolume * INT16_MAX)) / 2.0f); 
                    *SampleOut++ = (int16) ((*SampleOut + (-a_Oscillator.ToneVolume * INT16_MAX)) / 2.0f); 
                }
            }
            
            ++a_Oscillator.RunningSampleIndex;
        }
        
        // Decrementing the total running sampleindex by whole waveperiods so it never wraps around.
        // Remember that we only care about the offset from a complete waveperiod.
        // And we only do this for the square because its the only waveform so far that depends on 
        // a running sampleindex.
        uint32 nWavePeriodsInRunningSampleIndex = (a_Oscillator.RunningSampleIndex / nSamplesPerWavePeriod);
        a_Oscillator.RunningSampleIndex -= (nWavePeriodsInRunningSampleIndex * nSamplesPerWavePeriod);
    }
}

/// [Deprecated]
internal void
ApplyVolumeEnvelope(IN real64 a_AttackTimeMs, 
                    IN OUT real64& a_MsLeft, 
                    IN real64 a_MiliSecondsToProgress, 
                    IN OUT game_sound_out& a_SoundBuffer)
{
    // Calculate by how much the volume should decrement per value.
    real64 AttackVolumeDecrementValuePerSample = a_MiliSecondsToProgress / a_SoundBuffer.SampleCount;  
    
    // Point to the start of the game soundbuffer.
    int16* SampleOut = (int16*)a_SoundBuffer.BufferMemory;
    
    for(uint32 SampleIndex = 0; SampleIndex < a_SoundBuffer.SampleCount; ++SampleIndex)
    {
        // The attack time volume the maximum volume (1.0f) minus the coefficient 
        // of where we are in the attack time. 
        real32 AttackTimeVolume = 1.0f - ((real32)a_MsLeft / (real32)a_AttackTimeMs);
        
        //Left
        *SampleOut++ = (int16)((real32)*SampleOut * AttackTimeVolume);
        //Right
        *SampleOut++ = (int16)((real32)*SampleOut * AttackTimeVolume);
        
        // Safely decrement the amount of ms left for the attack envelope to be used in the next write.
        if((a_MsLeft - AttackVolumeDecrementValuePerSample) > 0)
        {
            a_MsLeft -= AttackVolumeDecrementValuePerSample;
        }
        else
        {
            a_MsLeft = 0;
        }
    }
}

/// Is the note fully played? 
/// This is used as a predicate for std::remove_if in 'PlayScheduledNotes'
internal bool32
IsFullyPlayed(game_scheduled_note a_Note)
{
    //If it is, its DurationMs is zero. Flip the flag; zero is true. 
    bool32 Result = (bool32)(a_Note.DurationMs); 
    return (!Result);
}

/// Writes the audio buffer for any notes that exist in "ScheduledNotes", and mixes them 
/// with the existing buffer memory.
/// @param a_SamplesPerSecond The amount samples per second for the buffer.
/// @param a_NextFrameSampleCount How many samples do we need to write this frame?
/// @param a_BufferMemory The sound buffer memory to output the result to.
internal void 
PlayScheduledNotes(IN uint32 a_SamplesPerSecond,IN uint32 a_NextFrameSampleCount,OUT void* a_BufferMemory)
{
    // Only write something we need to write any samples at all.
    if(a_NextFrameSampleCount)
    {
    if(!ScheduledNotes.empty())
    {
        uint32 NoteIndex = 0;
        for(auto& Note:ScheduledNotes) 
        {
            real32 SamplesPerMs = a_SamplesPerSecond / 1000.0f;
            // Times 1000 to go from seconds to miliseconds.
            real32 FrameDurationMs = ((real32)a_NextFrameSampleCount / a_SamplesPerSecond) * 1000.0f; 
             
            // How many ms of offset do we have this frame?
            real32 MsOffsetThisFrame = 0;
            
            if(Note.OffsetMs > FrameDurationMs)
            {
                MsOffsetThisFrame = Note.OffsetMs;
                Note.OffsetMs -= FrameDurationMs;
            }
            else
            {
                MsOffsetThisFrame = Note.OffsetMs;
                Note.OffsetMs = 0;
            }
            
            
            // How many miliseconds of waveform should we write this frame.
            real32 MsToWriteThisFrame = 0;
            
            if(MsOffsetThisFrame < FrameDurationMs)
            {
                // How many miliseconds do we have left to write actual waveforms?
                real32 MsLeftAfterOffset = FrameDurationMs - MsOffsetThisFrame;
                
                if(Note.DurationMs < MsLeftAfterOffset)
                {
                    MsToWriteThisFrame = Note.DurationMs;
                    Note.DurationMs = 0;
                }
                else
                {
                    MsToWriteThisFrame = MsLeftAfterOffset;
                    Note.DurationMs -= MsLeftAfterOffset;
                }
            }
            
                Assert(Note.DurationMs >= 0.0f);
                
            uint32 SamplesOffsetThisFrame = (uint32)(MsOffsetThisFrame * SamplesPerMs);
            uint32 SamplesToWriteThisFrame = (uint32)(MsToWriteThisFrame * SamplesPerMs);
                
            // In the case that we have an uneven "a_NextFrameSampleCount", and the amount of ms offset is exactly the same
            // as the ms to write, we have to make up for rounding errors.
            if((SamplesOffsetThisFrame == SamplesToWriteThisFrame)
               && (SamplesOffsetThisFrame + SamplesToWriteThisFrame) == (a_NextFrameSampleCount -1))
            {
                 SamplesToWriteThisFrame += 1;
                Assert((SamplesOffsetThisFrame + SamplesToWriteThisFrame) == a_NextFrameSampleCount);
            }
                
            // Overwrite the buffer for the first note; otherwise we're mixing with an empty buffer
            // which effectively just makes the first note half the volume.
            // TODO(Berend): Study if "additive" is really right?
            e_WriteMode WriteMode = (NoteIndex == 0 ? e_WriteMode::Overwrite : e_WriteMode::Additive);
            
                OscillatorWriteBuffer(a_SamplesPerSecond, SamplesToWriteThisFrame, SamplesOffsetThisFrame, WriteMode, Note.OscillatorSettings, a_BufferMemory); 
            
            ++NoteIndex;
        }
        }
        
        // Remove any notes that are fully played.
        ScheduledNotes.erase(std::remove_if(ScheduledNotes.begin(), ScheduledNotes.end(), IsFullyPlayed), ScheduledNotes.end()); 
    }
}

/// Returns the frequency for a given piano key on a standard 88 key piano. 
/// TODO(Berend): Allow to supply octaves and key enums such as 'C'. 
internal real32
GetKeyFrequency(IN uint16 a_KeyNumber)
{
    real32 Result = 0.0f;
    
    //----- Calculate the frequency as an offset from A4. -----
    uint16 A4Frequency = 440; 
    real64 HalfNoteMultiplier = pow((2.0f), 1/12.0f);
    int16 KeyOffSet = a_KeyNumber - 49;// A4 is the 49th key.
    real64 KeyFrequency = A4Frequency * (pow(HalfNoteMultiplier, KeyOffSet)); 
    
    // Only return positive frequencies.
    if(KeyFrequency > 0.0f)
        {
            Result = (real32)KeyFrequency;
        }
        
        return Result;
}

/// 'Updates' the synthesizer by writing the notes for this frame to a buffer,
/// and outputs the result in a_SoundBuffer.
internal void
UpdateSynth(OUT game_sound_out& a_SoundBuffer)
{
    // Overwrite whatever is in the buffer with silence.
    SecureZeroMemory(a_SoundBuffer.OscMemoryB, a_SoundBuffer.SamplesPerSecond * 4);
    
    game_osc_mixer SoundMixer = {};
    SoundMixer.VolumeOscA = 1.0f;
    SoundMixer.VolumeOscB = 1.0f;
    
    // Output the scheduled notes to a_SoundBuffer.
    PlayScheduledNotes(a_SoundBuffer.SamplesPerSecond, a_SoundBuffer.SampleCount, a_SoundBuffer.OscMemoryB);

    // Mix two oscillators to the main soundbuffer memory.
// For now it mixes still mixes with itself. 
    MixOscillators(SoundMixer, a_SoundBuffer.OscMemoryB, a_SoundBuffer.OscMemoryB, a_SoundBuffer);
}
