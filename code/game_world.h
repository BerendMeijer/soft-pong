     #pragma once
#include <vector>
     
     enum e_componenttype {NullComponent, Rigidbody2D, ConvexCollider2D}; 
     
     /// The base type for all components.
     struct game_component
     {
         bool32 IsEnabled;
         e_componenttype Type;
     };
     
     /// A 2D Rigidbody component that gets handled by 
     /// the physics engine.
     struct game_rigidbody2D : public game_component
     {
         game_vec2 Velocity;
         game_vec2 Acceleration;
         real32 Mass; //< In kilograms. A mass of 0 means that it is static.
         real32 Drag; //< Min drag = 0. Max drag = 1.
         bool32 IsKinematic;
     };
     
     struct game_convex_collider2D : public game_component
     {
         game_vertex_list VertexList; //< A list of vertices that make up the convex collider. 
         // These are relative to the origin of the object it is attached to.
         // Store them in clockwise order. 
     };
     
struct game_object
     {
         // TODO(Berend): Transform component.
         game_vec2 Position;
         real32 Width;
         real32 Height;
         game_color Color;
         
         std::vector<game_component*> Components;
     };
     
     
     struct game_world_objects
     {
         std::vector<game_object*> GameObjects;
         
         // TODO(Berend): Get rid of there and get pointers to gameobjects by name. 
         game_object* PlayerOne;
         game_object* PlayerTwo;
         game_object* PongBall;
         game_object* TopLevelBounds;
         game_object* BottomLevelBounds;
         game_object* TopTriangle;
         game_object* BottomTriangle;
         
         game_file* BackgroundImageFile;
         game_file* LeftPadImageFile;
         game_file* BottomTriangleImageFile;
         game_file* TopTriangleImageFile;
         game_file* PongballImageFile;
     };
     
     
     