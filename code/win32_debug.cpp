/// Draws a waveform and play and writecursor to a bitmap.
internal void
Win32DebugDrawWaveForm(IN const game_bitmap& a_Bitmap, 
                       IN const win32_debug_playwritecursors& a_PlayWriteCursors, 
                       IN const win32_sound_output& a_WinSoundOutput, 
                       IN uint16 a_GameUpdateFrequency, 
                       IN game_sound_out a_GameSoundBuffer)
{
    #if 0
    uint32 OneFrameSampleCount = (a_WinSoundOutput.SamplesPerSecond/a_GameUpdateFrequency);
    
    uint32 OneFrameByteCount = (OneFrameSampleCount * a_WinSoundOutput.BytesPerSample);
    // Calculate difference between the writecursor and the next frame boundary.
    uint32 BytesToNextFrameBoundary = (OneFrameByteCount - (a_PlayWriteCursors.WriteCursor % OneFrameByteCount));
    
    uint32 ByteToWriteFrom = a_WinSoundOutput.ByteToLock % a_WinSoundOutput.SecondaryBufferSize;
    
    // Calculate how the sample that we're writing from in the soundbuffer maps to the screen.
    real32 SampleToWriteFromCoefficient = ((real32)a_WinSoundOutput.SecondaryBufferSize / ByteToWriteFrom);
    // The amount of pixels per sample in screen width.
    real32 XPixelsPerSampleCoefficient = ((real32) a_Bitmap.Width/ a_WinSoundOutput.SamplesPerSecond);
    real32 YPixelsPerSampleCoefficient = (real32) a_Bitmap.Height / UINT16_MAX;
    
    real32 XPixelToWriteFrom = ((real32)a_Bitmap.Width / SampleToWriteFromCoefficient);
    
    // Point to the start of the game output buffer. 
    int16* SampleSource = (int16*) a_GameSoundBuffer.BufferMemory;
    
    // Point to the first pixel.
    uint32* Pixel = (uint32*)a_Bitmap.Memory; 
    for(uint32 SampleIndex = 0; SampleIndex < a_GameSoundBuffer.SampleCount; ++SampleIndex)
    {
        uint32 PixelXToWriteTo = (uint32)(XPixelToWriteFrom + (XPixelsPerSampleCoefficient * SampleIndex));
        // Write from the middle line.
        int32 YPixelHeight = (int32)(((*SampleSource) * YPixelsPerSampleCoefficient));
        int32 YOffset = (int32)(YPixelHeight * a_Bitmap.Width);
        int32 PixelYToWriteTo = (int32)((((real32)a_Bitmap.Height / 2.0f) * a_Bitmap.Width) -  YOffset); 
        
        uint32 PixelOffset = PixelXToWriteTo + PixelYToWriteTo;
        uint8* TargetPixel = (uint8*)(Pixel + PixelOffset);
        
        *TargetPixel++ = 255; //Blue
        *TargetPixel++ = 0; //Green
        *TargetPixel++ = 50; //Red
        TargetPixel++; // Unused.
        
        SampleSource+= 2;
    }
    #endif
}


internal void
Win32DebugDrawWriteCursor(IN game_bitmap& a_Bitmap, IN win32_debug_playwritecursors a_PlayWriteCursors, IN uint32 a_BytesPerSecond)
{
    real32 WriteCursorCoefficient = ((real32)a_BytesPerSecond / (real32)a_PlayWriteCursors.WriteCursor); 
    uint32 WriteCursorPosition = (uint32)((real32)a_Bitmap.Width/(real32)WriteCursorCoefficient);
    
    real32 PlayCursorCoefficient = ((real32)a_BytesPerSecond / (real32)a_PlayWriteCursors.PlayCursor); 
    uint32 PlayCursorPosition = (uint32)((real32)a_Bitmap.Width/(real32)PlayCursorCoefficient);
    
    int32 BytesPerRow = (int32)(a_Bitmap.Width * a_Bitmap.BytesPerPixel);
    
    // Point to the first pixel.
    uint32* Pixel = (uint32*)a_Bitmap.Memory; 
    
    uint8* PixelColour = (uint8*)Pixel;
    
    for(uint16 Yiterator = 0; Yiterator < a_Bitmap.Height; ++Yiterator)
    {
        for(uint16 Xiterator = 0; Xiterator < a_Bitmap.Width; ++Xiterator) 
        {
            if(Xiterator == PlayCursorPosition)
            {
                *PixelColour++ = 0; //Blue
                *PixelColour++ = 0; //Green
                *PixelColour++ = 255; //Red
                PixelColour-= 3; // Unused.
            }
            
            if(Xiterator ==  WriteCursorPosition)
            {
                *PixelColour++ = 0; //Blue
                *PixelColour++ = 255; //Green
                *PixelColour++ = 0; //Red
                PixelColour++; // Unused.
            }
            else
            {
                PixelColour += 4;
            }
            
        }
        // Point to the next row.
        Pixel += BytesPerRow;
    }
}

internal void
Win32DebugDrawFrameBoundaries(IN game_bitmap& a_Bitmap,IN uint32 a_OneFrameSampleCount,IN uint32 a_SamplesPerSecond)
{
    real32 FirstFrameBoundaryCoefficient = ((real32)a_SamplesPerSecond / (real32)a_OneFrameSampleCount);
    uint32 FirstFrameBoundary = (uint32)((real32)a_Bitmap.Width / FirstFrameBoundaryCoefficient); 
    
    int32 BytesPerRow = (int32)(a_Bitmap.Width * a_Bitmap.BytesPerPixel);
    
    // Point to the first pixel.
    uint32* Pixel = (uint32*)a_Bitmap.Memory; 
    
    uint8* PixelColour = (uint8*)Pixel;
    
    for(uint16 Yiterator = 0; Yiterator < a_Bitmap.Height; ++Yiterator)
    {
        for(uint16 Xiterator = 0; Xiterator < a_Bitmap.Width; ++Xiterator) 
        {
            if(Xiterator % FirstFrameBoundary == 0)
            {
                *PixelColour++ = 200; //Blue
                *PixelColour++ = 160; //Green
                *PixelColour++ = 50; //Red
                PixelColour++; // Unused.
            }
            else
            {
                PixelColour += 4;
            }
        }
        // Point to the next row.
        Pixel += BytesPerRow;
    }
}