#pragma pack(push, 2)  
struct win32_bitmap_info
{
    BITMAPFILEHEADER FileHeader;
    BITMAPV5HEADER BitmapHeader;
};
#pragma pack(pop)  

struct game_file
{
    uint32 FileSize; //< The size of the file in bytes. Has a max of 4 Gigabytes.
    void* FileMemory; //< Pointer to the memory.
    
    game_bitmap ImageData;
};
