/// Initializes OpenGL.
internal bool32 
Win32InitOpenGL(IN HDC a_DeviceContext,OUT HGLRC& a_OpenGLDeviceContext)
{
    bool32 Result  = false; // Return value.
    
    PIXELFORMATDESCRIPTOR RequestedPixelFormat = {};
    int PixelFormatIndex = 0;
    
    RequestedPixelFormat.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    RequestedPixelFormat.nVersion = 1;
    RequestedPixelFormat.dwFlags = PFD_DRAW_TO_WINDOW |
        PFD_DOUBLEBUFFER|PFD_SUPPORT_OPENGL;
    RequestedPixelFormat.iPixelType = PFD_TYPE_RGBA;
    RequestedPixelFormat.cColorBits = 24;
    RequestedPixelFormat.cAlphaBits = 8;
    
    // Returns zero if the function fails.
    PixelFormatIndex =  ChoosePixelFormat(a_DeviceContext, &RequestedPixelFormat);
    
    if(PixelFormatIndex)
    {
        PIXELFORMATDESCRIPTOR SuppliedPixelFormat = {};
        DescribePixelFormat(a_DeviceContext, PixelFormatIndex, 
                            sizeof(PIXELFORMATDESCRIPTOR), &SuppliedPixelFormat);
        
        if(SetPixelFormat(a_DeviceContext, PixelFormatIndex , &SuppliedPixelFormat))
        {
            a_OpenGLDeviceContext = wglCreateContext(a_DeviceContext);
            // Make the OpenGL rendering context the context for this thread.
            if(wglMakeCurrent (a_DeviceContext, a_OpenGLDeviceContext))
            {
                Result= true;
            }
            else
            {
                OutputDebugStringA("Couldnt set OpenGL as devicecontext");
            }
        }
        else
        {
            OutputDebugStringA("Failed to set pixel format");
        }
    }
    else
    {
        OutputDebugStringA("Failed to choose a pixel format");
    }
    
    return (Result);
}

/// Sets the bitmap size and allocates and/or frees the memory for the bitmap.
internal void
Win32SetBitmapSize(IN game_bitmap& a_GameBitmap, 
                   IN uint32 a_Width, 
                   IN uint32 a_Height)
{
    a_GameBitmap.Width = a_Width;
    a_GameBitmap.Height = a_Height;
    
    uint32 BitmapBytes = (uint32)((a_Width * a_Height) * a_GameBitmap.BytesPerPixel);
    
    if(a_GameBitmap.Memory)
    {
        VirtualFree(a_GameBitmap.Memory, 0, MEM_RELEASE);
    }
    
    // Get a block of memory, the size of the bitmap.
    a_GameBitmap.Memory = VirtualAlloc(0, BitmapBytes, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
}


/// - Gets the client window dimensions.
/// - Draws the bitmap to the screen using StretchDIBits.
internal void
Win32RenderBitmapToScreen(IN const game_bitmap & a_GameBitmap, IN HDC a_DeviceContext, IN  HWND a_WindowHandle)
{
    // Get window dimensions.
    RECT WindowRect = {};
    int WindowWidth = 0;
    int WindowHeight = 0;
    if(GetClientRect(a_WindowHandle, &WindowRect))
    {
        WindowWidth = WindowRect.right - WindowRect.left;
        WindowHeight = WindowRect.bottom - WindowRect.top;
    }
    else
    {
        OutputDebugStringA("Couldnt get window rect");
    }
    
    BITMAPINFOHEADER BitMapHeader = {};
    BitMapHeader.biSize = sizeof(BitMapHeader);
    BitMapHeader.biWidth = (LONG)a_GameBitmap.Width;
    // Making the biHeight positive indicates that the left bottom corner is screen origin (0,0).
    // And the starting value of bitmap memory is displayed there. 
    BitMapHeader.biHeight = (LONG)a_GameBitmap.Height;
    BitMapHeader.biPlanes = 1;
    BitMapHeader.biBitCount = 32;
    BitMapHeader.biCompression = BI_RGB;
    
    BITMAPINFO BitmapInfo = {};
    BitmapInfo.bmiHeader = BitMapHeader;
    
    // Draw bitmap to screen.
    StretchDIBits(a_DeviceContext, 10, 10, (int)a_GameBitmap.Width, (int)a_GameBitmap.Height, // Target size. 
                  0,0, (int)a_GameBitmap.Width, (int)a_GameBitmap.Height, 
                  a_GameBitmap.Memory, 
                  &BitmapInfo, DIB_RGB_COLORS, SRCCOPY);
}
