/// Compares two floats taking a precision error.
/// Uses the algorithm from:
/// https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
/// TODO: Study other examples (from well established math books) and check if this is right. 
inline bool32
AreEqual(IN real32 a_FloatA,IN real32 a_FloatB,
         IN real32 MaxRelativeDifference = R32_EPSILON)
{
    real32 Difference = (real32)fabs(a_FloatA - a_FloatB);
    a_FloatA = (real32)fabs(a_FloatA);
    a_FloatB = (real32)fabs(a_FloatB);
    
    real32 Largest = (a_FloatB > a_FloatA) ? a_FloatB : a_FloatA;
    
    if(Difference <= (Largest * MaxRelativeDifference))
    {
        return true;
    }
    else
    {
        return false;
    }
}


/// Copies over a null terminated string from a_Source to a_Destination. 
/// It does so by copying over characters from a_Source to a_Destination until it reaches a 0.
/// No valid memory/bounds checking! 
internal void
CopyCString(IN char* a_Source, OUT char* a_Destination)
{
    uint32 Iterator = 0;
    
    while(a_Source[Iterator])
    {
        a_Destination[Iterator] = a_Source[Iterator];
        ++Iterator;
    }
}

/// Returns true when the Strings contain exactly the same characters
/// from the start until a null character is found.
internal bool32 
CompareCString(IN char* a_StringA, IN char* a_StringB)
{
    bool32 Result = true;
    uint32 Iterator = 0;
    
    // While we haven't reached the '0' terminator in the first string, keep checking characters.
    // The result is set to false if there ever is a character that wasnt the same.
    while(a_StringA[Iterator])
    {
        if(a_StringA[Iterator] == a_StringB[Iterator])
        {
            
        }
        else
        {
            Result = false;
        }
        
        ++Iterator;
    }
    
    // Check if both strings are at the null terminator.
    // In the case that StringA is shorter than StringB, the result is also false.
    if(!(a_StringA[Iterator] == a_StringB[Iterator]))
    {
        Result = false;
    }
    
    return Result;
}


