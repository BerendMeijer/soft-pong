/// Looks for latest created game DLL name in the directory, and returns the result in a_DLLName.
/// The result can be used to load the DLL by name.
internal void
Win32GetLatestGameDLLName(OUT char* a_DLLName)
{
    WIN32_FIND_DATA SearchResult = {};
    
    // Searches the build directory because that is the directory that is set
    // by default as working directory in visual studio.
    // TODO(Berend): Study how this works when the game is run on a different computer.
    HANDLE DLLHandle =  FindFirstFile("game*.dll", &SearchResult);
    
    FILETIME HighestFileTimeUntoNow = SearchResult.ftCreationTime; 
    
    char* LatestDLLName = SearchResult.cFileName;
    while(FindNextFile(DLLHandle, &SearchResult))
    {
        LONG CompareResult = CompareFileTime(&HighestFileTimeUntoNow,
                                             &SearchResult.ftCreationTime);
        
        // First time is earlier than second time.
        if(CompareResult == -1)
        {
            HighestFileTimeUntoNow = SearchResult.ftCreationTime;
            LatestDLLName = SearchResult.cFileName;
        }
        // Times are the same.
        else if(CompareResult == 0)
        {
            // This is never supposed to happen.
            Assert(0);
        }
    }
    FindClose(DLLHandle);
    
    CopyCString(LatestDLLName, a_DLLName);
}

internal uint32
Win32GetFileSize(IN char* a_FileName)
{
    uint32 Result = 0;
    
    // Create file i/o device
    HANDLE FileIOHandle =  CreateFile(a_FileName, GENERIC_READ, 
                                      FILE_SHARE_READ, 0, 
                                      OPEN_EXISTING, 
                                      FILE_ATTRIBUTE_NORMAL, 0); 
    
    if(FileIOHandle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER FileSize;
        
        if(GetFileSizeEx(FileIOHandle, &FileSize) && (CloseHandle(FileIOHandle)))
        {
            // Make sure that the file that we are trying to read isn't larger than 4 Gigabytes.
            // ReadFile only accepts a unsigned 32bit integer, so if we want to read larger files
            // it would have to be done in two times.
            Assert(FileSize.QuadPart <= UINT32_MAX);
            
             Result = (uint32)FileSize.QuadPart;
}
}

return Result;
}

/// Rearranges a BMP file in the colorlayout the the game expects: ARGB.
/// BMP files have varying layouts.
/// We only support uncompressed bitmaps for now.
internal void
Win32RearrangeBMPBits(IN void* a_FileMemory)
{
    win32_bitmap_info* BitmapInfo = (win32_bitmap_info*)a_FileMemory;
    
    // The bitmasks are only valid when  is set to BI_BITFIELDS.
    Assert(BitmapInfo->BitmapHeader.bV5Compression == BI_BITFIELDS);
    
    // Point to the first pixel in the BMP image.
    uint8* BitsAddress = ((uint8*)a_FileMemory) + BitmapInfo->FileHeader.bfOffBits;
    uint32* Pixel = (uint32*)BitsAddress;
    
    // Find the first bit of the masks so we can shift the actual set bits to the right place.
    DWORD BlueBitIndex = 0;
    DWORD GreenBitIndex = 0;
    DWORD RedBitIndex = 0;
    DWORD AlphaBitIndex = 0;
    
    _BitScanForward(&BlueBitIndex, BitmapInfo->BitmapHeader.bV5BlueMask);
    _BitScanForward(&GreenBitIndex, BitmapInfo->BitmapHeader.bV5GreenMask);
    _BitScanForward(&RedBitIndex, BitmapInfo->BitmapHeader.bV5RedMask);
    _BitScanForward(&AlphaBitIndex, BitmapInfo->BitmapHeader.bV5AlphaMask);
    
    // Move the bits to the target memory layout: ARGB.
    for(uint16 Yiterator = 0; Yiterator < BitmapInfo->BitmapHeader.bV5Height; ++Yiterator)
    {
        for(uint16 Xiterator = 0; Xiterator < BitmapInfo->BitmapHeader.bV5Width; ++Xiterator) 
        {
            //--------------------- Blue bits ------------------------------------
            // Filter out the bits that we need.
            uint32 BlueBits = (*Pixel & BitmapInfo->BitmapHeader.bV5BlueMask);
            
            // Where do we want the first bit? (Going from least significant to most significant bit)
            // The target memory layout is: ARGB
            uint32 BlueBitsTargetIndex = 0;
            if(BlueBitIndex > BlueBitsTargetIndex)
            {
                // The bits are set to the right of the actual index where we want them to be. 
                // Shift to the left.
                uint32 BitsToShift = BlueBitIndex - BlueBitsTargetIndex;
                BlueBits  = (BlueBits >> BitsToShift);
            }
            else
            {
                // If the bits set are to the left, we move them to the right.
                // Also, if they are in the right place we'll do a bitshift by 0.
                uint32 BitsToShift = BlueBitsTargetIndex - BlueBitIndex;
                BlueBits = (BlueBits << BitsToShift);
            }
            
            
            //--------------------- Green bits ------------------------------------
            // Filter out the bits that we need.
            uint32 GreenBits = (*Pixel & BitmapInfo->BitmapHeader.bV5GreenMask);
            
            // Where do we want the first bit? (Going from least significant to most significant bit)
            // The target memory layout is: ARGB
            uint32 GreenBitsTargetIndex = 8;
            if(GreenBitIndex > GreenBitsTargetIndex)
            {
                // The bits are set to the right of the actual index where we want them to be. 
                // Shift to the left.
                uint32 BitsToShift = GreenBitIndex - GreenBitsTargetIndex;
                GreenBits  = (GreenBits >> BitsToShift);
            }
            else
            {
                // If the bits set are to the left, we move them to the right.
                // Also, if they are in the right place we'll do a bitshift by 0.
                uint32 BitsToShift = GreenBitsTargetIndex - GreenBitIndex;
                GreenBits = (GreenBits << BitsToShift);
            }
            
            //------------------- Red bits ---------------------------------------
            // Filter out the bits that we need.
            uint32 RedBits = (*Pixel & BitmapInfo->BitmapHeader.bV5RedMask);
            
            // Where do we want the first bit? (Going from least significant to most significant bit)
            // The target memory layout is: ARGB
            uint32 RedBitsTargetIndex = 16;
            if(RedBitIndex > RedBitsTargetIndex)
            {
                // The bits are set to the right of the actual index where we want them to be. 
                // Shift to the left.
                uint32 BitsToShift = RedBitIndex - RedBitsTargetIndex;
                RedBits  = (RedBits >> BitsToShift);
            }
            else
            {
                // If the bits set are to the left, we move them to the right.
                // Also, if they are in the right place we'll do a bitshift by 0.
                uint32 BitsToShift = RedBitsTargetIndex - RedBitIndex;
                RedBits = (RedBits << BitsToShift);
            }
            
            
            //--------------------- Alpha bits ------------------------------------
            // Filter out the bits that we need.
            uint32 AlphaBits = (*Pixel & BitmapInfo->BitmapHeader.bV5AlphaMask);
            
            // Where do we want the first bit? (Going from least significant to most significant bit)
            // The target memory layout is: ARGB
            uint32 AlphaBitsTargetIndex = 24;
            if(AlphaBitIndex > AlphaBitsTargetIndex)
            {
                // The bits are set to the right of the actual index where we want them to be. 
                // Shift to the left.
                uint32 BitsToShift = AlphaBitIndex - AlphaBitsTargetIndex;
                AlphaBits  = (AlphaBits >> BitsToShift);
            }
            else
            {
                // If the bits set are to the left, we move them to the right.
                // Also, if they are in the right place we'll do a bitshift by 0.
                uint32 BitsToShift = AlphaBitsTargetIndex - AlphaBitIndex;
                AlphaBits = (AlphaBits << BitsToShift);
            }
            
            // Or the resulting parts together, and overwrite the original pixel memory.
            uint32 Result = (BlueBits | GreenBits | RedBits | AlphaBits);
            *Pixel++ = Result;
        }
    }
}


/// Loads a file from the working directory or a specified path.
/// @a_FileName The name of the file.
/// @a_IsBMP Is the file a BMP image? Then we rearrange the order on load.
/// @a_GameFile The read result gets written into a_GameFile.
internal bool32 
Win32LoadFile(IN char* a_FileName, IN bool32 a_IsBMP, 
              IN uint32 a_FileSize, OUT void* a_FileMemory,
              OUT game_bitmap& a_ImageInfo)
{
    bool32 Result = false;
    
    Assert(a_FileMemory != 0);
    
    // Create file i/o device
    HANDLE FileIOHandle =  CreateFile(a_FileName, GENERIC_READ, 
                                      FILE_SHARE_READ, 0, 
                                      OPEN_EXISTING, 
                                      FILE_ATTRIBUTE_NORMAL, 0); 
    
    if(FileIOHandle != INVALID_HANDLE_VALUE)
    {
            DWORD BytesRead = 0;
            
            // Comparing BytesRead and FileSize to make sure that no bytes were lost during the read.
            if(ReadFile(FileIOHandle, a_FileMemory, a_FileSize, &BytesRead, 0) && BytesRead == a_FileSize) 
            {
                if(CloseHandle(FileIOHandle))
                {
                    Result = true;
                    
                    if(a_IsBMP)
                    {
                        Win32RearrangeBMPBits(a_FileMemory);
                        
                        // If it is a BMP, the win32_bitmap_info should be in front of the file memory.
                        // We will give the game the only things that it needs: width, height and a pointer to the start of memory.
                        win32_bitmap_info* BitmapInfo = (win32_bitmap_info*)a_FileMemory;
                        
                        uint8* ColorBitsAddress = ((uint8*)a_FileMemory) + BitmapInfo->FileHeader.bfOffBits;
                        
                        a_ImageInfo.Memory = ColorBitsAddress;
                        a_ImageInfo.Width = (uint32)BitmapInfo->BitmapHeader.bV5Width;
                        a_ImageInfo.Height = (uint32)BitmapInfo->BitmapHeader.bV5Height;
                    }
                }
                else
                {
                    OutputDebugStringA("Couldnt close file handle");
                }
            }
            else
            {
                OutputDebugStringA("Read file error");
            }
    }
    else
    {
        OutputDebugStringA("File not found");
    }
    // Is only true when the file was read and handle was closed succesfully.
    return Result;
}

/// Writes a file to disk. 
internal bool 
Win32WriteFile(IN char* a_FileName, IN game_file* a_GameFile)
{
    bool Result = false;
    
    // Create file i/o device
    HANDLE FileIOHandle =  CreateFile(a_FileName, GENERIC_WRITE, 
                                      0, 0, 
                                      CREATE_ALWAYS, 
                                      FILE_ATTRIBUTE_NORMAL, 0); 
    
    if(FileIOHandle != INVALID_HANDLE_VALUE)
    {
        // Make sure that the file that we are trying to write isn't larger than 4 Gigabytes.
        // WriteFile only accepts a unsigned 32bit integer, so if we want to write larger files
        // it would have to be done in two times.
        Assert(a_GameFile->FileSize  <= UINT32_MAX);
        
        DWORD BytesWritten = 0;
        
        if(WriteFile(FileIOHandle, a_GameFile->FileMemory, 
                     a_GameFile->FileSize,  
                     &BytesWritten, 0))
        {
            
            if(CloseHandle(FileIOHandle))
            {
                Result = true;
            }
            else
            {
                OutputDebugStringA("Couldnt close file handle");
            }
        }
        else
        {
            OutputDebugStringA("Write file error");
        }
    }
    // Is only true when the file was written and handle was closed succesfully.
    return Result;
}
