global_variable void* MemoryPointer;
global_variable uint32 MemoryByteCount;

/// 'Allocates' memory for an object on our own heap, and returns
/// a pointer to the start of the objectmemory. Its a region based.
/// Returns null if the allocation failed.
internal void*
PushObject(IN uint32 a_ByteSize)
{
    void* StartOfObjectMemory = 0;
    // Return null if the user is trying to allocate zero bytes.
    if(a_ByteSize > 0)
    {
    // Return a pointer to the start of the memory space.
    game_memory_state* MemoryState = (game_memory_state*)MemoryPointer;
    
    uint8* MemoryBytePointer = (uint8*)MemoryPointer;
    
    StartOfObjectMemory = (void*)(MemoryBytePointer + MemoryState->ByteCount);
    
    MemoryState->ByteCount += a_ByteSize;
    Assert(MemoryState->ByteCount < MemoryByteCount);
    }
    return StartOfObjectMemory;
}

internal void
SetMemoryPointer(void* a_Memory, uint32 a_MemoryByteCount)
{
    MemoryPointer = a_Memory;
    MemoryByteCount = a_MemoryByteCount;
}