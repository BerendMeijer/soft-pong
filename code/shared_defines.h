#include <stdint.h>
#include <float.h>

#define internal static
#define global_variable static 
#define local_persistent static
#define PI 3.14159265359f	
#define R32_EPSILON FLT_EPSILON 
#define IN /// Functions writes into a given parameter.
#define OUT /// Function reads from a given parameter.

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int32_t bool32;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef float real32;
typedef double real64;

// Replaces value by the number of bytes.
#define Kilobytes(Value) (Value * 1024); 
#define Megabytes(Value) (Value * (1024 * 1024)); 
#define Gigabytes(Value) (Value * (1024 * 1024 * 1024));

#if GAME_DEBUG
// Write to the nullptr to crash the program in debug build.
#define Assert(Expression)if(!(Expression)) {*(int*) 0 = 1;} 
#else
// Remove the assert when we are not debugging.
#define Assert(Expression)
#endif

