internal MSG
Win32ProcessWindowsMessages(IN HWND a_WindowHandle, IN HDC a_DeviceContext, OUT bool32& a_IsRunning)
{
    MSG msg = {};
    
    // PeekMessage doesnt stall the application waiting for new messages.
    while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
        UINT WindowMessage = msg.message;
        switch(WindowMessage)
        {
#if SOFTWARE_RENDERING
            case WM_PAINT:
            {
                PAINTSTRUCT ps;
                BeginPaint(a_WindowHandle, &ps);
                EndPaint(a_WindowHandle, &ps);
            }
            break;
#endif
            case WM_KEYDOWN:
            {
                WPARAM Keycode = msg.wParam;
                if(Keycode == VK_ESCAPE)
                {
                    PostQuitMessage(0);
                }
            }
            break;
            case WM_QUIT:
            {
                // Message comes in right after PostQuitMessage(0).
                // WM_QUIT is only retreived by PeekMessage and GetMessage functions.
                a_IsRunning = false;
            }
            break;
            
            default:
            // Sends the message to the callback. 
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            break;
        }
    }
    return msg;
}


LRESULT CALLBACK WndProc(HWND a_WindowHandle, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
#if SOFTWARE_RENDERING
        case WM_PAINT:
        {
            // Cathing this message here prevents Windows
            // from processing it in the DefWindowProc.
            // We dont want that because we're painting 
            // manually. 
            // Catching this message  in PeekMessage does not 
            // prevent it from being send here.
        }
        break;
#endif
        case WM_DESTROY:
        PostQuitMessage(0);
        break;
        
        case WM_KEYDOWN:
        OutputDebugStringA("Windows tried to process a keydown message!\n");
        break;
        
        default:
        return DefWindowProc(a_WindowHandle, message, wParam, lParam);
        break;
    }
    return 0;
}



internal bool32
Win32CreateGameWindow(IN HINSTANCE a_Instance, 
                      IN int a_ShowCode,
                      OUT HWND& a_WindowHandle)
{
    a_WindowHandle = 0;
    
    TCHAR szWindowClass[] = ("Synth Pong");
    TCHAR szTitle[] = ("Synth Pong");
    
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style          = 0;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = a_Instance;
    wcex.hIcon          = 0;
    wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = NULL;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = 0;
    
    
    if (!RegisterClassEx(&wcex))
    {
        return false;
    }
    
    // NULL: not used in this application
    a_WindowHandle = CreateWindow(
        szWindowClass,
        szTitle,
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT,
        600, 600, //< Initial windowsize.
        NULL,
        NULL,
        a_Instance,
        NULL
        );
    
    if (!a_WindowHandle)
    {
        return false;
    }
    
    ShowWindow(a_WindowHandle, a_ShowCode);
    UpdateWindow(a_WindowHandle);
    
    // If we get here, the window was created succesfully.
    return true;
}



DWORD WindowMessagingThread(LPVOID lpParameter)
{
    win32_window_thread_parameters* WindowInfo = (win32_window_thread_parameters*)lpParameter;
    
    //----- Create a gamewindow. -----
    HWND WindowHandle = 0;
    HDC DeviceContext = 0; 
    if(Win32CreateGameWindow(WindowInfo->ProcessInstance, WindowInfo->ShowCode, WindowHandle))
    {
        DeviceContext = GetDC(WindowHandle);
    }
    else
    {
        WindowInfo->WindowCreationError = true;
        // Terminate the thread if something went wrong in the creation of the window.
        return EXIT_FAILURE;
    }
    
    // Set WindowIsReady flag to true so the game can start running. 
    WindowInfo->WindowIsReady = true;
    WindowInfo->WindowHandle = WindowHandle;
    WindowInfo->DeviceContext = DeviceContext;
    
    // Process  window messages for as long as the game runs.
    MSG WindowMessage = {};
    while(WindowInfo->IsRunning)
    {
        WindowMessage = Win32ProcessWindowsMessages(WindowHandle, DeviceContext, WindowInfo->IsRunning);
    }
    
    OutputDebugStringA("Windows message thread exits\n");
    return (DWORD)WindowMessage.wParam;
}



