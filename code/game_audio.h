struct game_sound_out
{
    uint32 SampleCount; //< Amount of 16 bit samples to write, stereo interleaved.
    uint32 SamplesPerSecond;
    void* BufferMemory; //< Points to the start of the memoryblock to write to.
    void* OscMemoryA; //< Oscillators use these buffers to store their outputs temporarily before mixing.
    void* OscMemoryB;
};

enum e_waveform {SINE, SQUARE};

// Envelope times in miliseconds.
struct game_synth_envelope
{
    real32 AttackTime; 
    real32 DecayTime;
    real32 SustainTime;
    real32 ReleaseTime;
};

struct game_osc
{
     real32 Frequency;
    real32 tSine; //< Needs to persist between frames to write a continuous sine wave.
    uint32 RunningSampleIndex; //< Needs to persist between frames so we can write a continuous squarewave.
     real32 ToneVolume;
    e_waveform WaveForm;
    game_synth_envelope AmpEnvelope;
};

struct game_osc_mixer
{
    real32 VolumeOscA;
    real32 VolumeOscB;
};
     