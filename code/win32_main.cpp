#include <math.h>

#include "shared_defines.h"
#include "shared_helpers.cpp"


// Headers that are only for this file.
#include <windows.h>
#include <initguid.h>
#include <dsound.h>
#include <xinput.h>
#include <stdio.h>
#include <GL/gl.h> 
#include <GL/glu.h> 
#include <Audiopolicy.h>
#include <Audioclient.h>
#include <Mmdeviceapi.h>


#include "game_main.h"

#include "win32_window.h"
#include "win32_debug.h"
#include "win32_sound.h"
#include "win32_input.h"

#include "win32_debug.cpp"
#include "win32_time.cpp"
#include "win32_fileio.cpp"
#include "win32_graphics.cpp"
#include "win32_window.cpp"
#include "win32_stubs.cpp"
#include "win32_input.cpp"
#include "win32_sound.cpp"



// A function pointer typedef to GameUpdate.
// Necessary because we always call the game through a function pointer.
// We get the pointer address of GameUpdate in the game DLL through GetProcAddress.
typedef void (__cdecl *GAMEUPDATEPTR)(IN const game_input &a_Input, 
                                      IN game_memory& a_GameMemory, 
                                      OUT game_sound_out& a_GameSoundBuffer, 
                                      OUT game_bitmap& a_GameBitmap);



int CALLBACK WinMain(HINSTANCE a_Instance,
                     HINSTANCE a_PreviousInstance,
                     LPSTR a_CommandLine,
                     int a_ShowCode)
{
    
    win32_sound_output WinSoundOutput = {};
    
    // This is the format that we're requesting. We will know the actual format
    // after calling Win32InitWASAPI, which writes the result to WinSoundOutput.
    WinSoundOutput.BitsPerSample = 16;
    WinSoundOutput.SamplesPerSecond = 44100;
    
    IAudioClient* AudioClient = 0;
    IAudioRenderClient* AudioRenderClient = 0;
    Win32InitWASAPI(WinSoundOutput, &AudioClient, &AudioRenderClient);
    
    // Calculate the buffersize in bytes. 8 bits per byte, two channels.
    WinSoundOutput.SecondaryBufferSize = (uint32)(WinSoundOutput.SamplesPerSecond * (WinSoundOutput.BitsPerSample / 8) * 2);
    
    if(AudioClient)
    {
    AudioClient->Start();
    }
    
    GAMEUPDATEPTR GameUpdatePtr = 0;
    
    win32_window_thread_parameters WindowInfo = {};
    WindowInfo.ProcessInstance = a_Instance;
    WindowInfo.ShowCode = a_ShowCode;
    WindowInfo.IsRunning = true;
    
    DWORD ThreadID = 0;
    HANDLE WindowsMessagingThreadHandle = CreateThread(0, // Thread handle cannot be inherited by child processes.
                                                       0, // Default stack size.
                                                       WindowMessagingThread,
                                                       &WindowInfo, // No parameter yet.
                                                       0, // Run immmidiately after creation.
                                                       &ThreadID);
    
    if(!WindowsMessagingThreadHandle)
    {
        OutputDebugStringA("Windows messaging thread creation failed");
        return EXIT_FAILURE;
    }
    
    // Spin wait until the window is ready to be used.
    while(!WindowInfo.WindowIsReady)
    {
        // Exit if there was an error.
        // The WindowIsReady flag never gets set to true when there was an error. 
        if(WindowInfo.WindowCreationError)
        {
            OutputDebugStringA("Window thread failed at creation.");
            return EXIT_FAILURE;
        }
    }
    
    if(SetPriorityClass(GetCurrentProcess(),HIGH_PRIORITY_CLASS))
    {
        
    }
    else
    {
        OutputDebugStringA("Failed to set process priority");
    }
    
    
    #if OPENGL
    HGLRC OpenGLDeviceContext = {};
    if(!Win32InitOpenGL(DeviceContext, OpenGLDeviceContext))
    {
        OutputDebugStringA("Couldnt Init OpenGL");
    }
    
    const GLubyte* OpenGLSpecs =   glGetString(GL_VERSION);
    OutputDebugStringA("OpenGL Version:  ");
    OutputDebugStringA((LPCSTR)OpenGLSpecs);
    OutputDebugStringA("\n");
    
     OpenGLSpecs= glGetString(GL_EXTENSIONS); 
    OutputDebugStringA("OpenGL Extensions:  ");
    OutputDebugStringA((LPCSTR)OpenGLSpecs);
    OutputDebugStringA("\n");
    #endif
    
    //----- Create bitmap ----
    game_bitmap GameBitmap = {};
    GameBitmap.BytesPerPixel = 4;
    // Initializes the bitmap with memory.
    Win32SetBitmapSize(GameBitmap, 500, 500);
    
    //----- Create game sound buffer ----
     game_sound_out GameSoundBuffer = {};
    // Get a backbuffer the size of the entire soundbuffer, for the game to write sound to. 
    GameSoundBuffer.BufferMemory = VirtualAlloc(0, WinSoundOutput.SecondaryBufferSize,
                                            MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);  
    
    GameSoundBuffer.OscMemoryA = VirtualAlloc(0, WinSoundOutput.SecondaryBufferSize,
                                                MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);  
    
    GameSoundBuffer.OscMemoryB = VirtualAlloc(0, WinSoundOutput.SecondaryBufferSize,
                                                MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);  
    
    GameSoundBuffer.SamplesPerSecond = WinSoundOutput.SamplesPerSecond;
    
     //----- Initialize Xinput -----
     Win32InitXInput();
     
     //---- Initialize game memory -----
    game_memory GameMemory = {};
    GameMemory.PermanentMemorySize = Megabytes(64);
    GameMemory.TransientMemorySize = Gigabytes(1);
    
    GameMemory.PermanentMemory = VirtualAlloc(0, GameMemory.PermanentMemorySize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
    GameMemory.TransientMemory = VirtualAlloc(0, GameMemory.TransientMemorySize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
    
    
#if GAME_DEBUG
    win32_debug_playwritecursors PlayWriteCursors = {};
    #endif
    
    //---- Set time precision to max ----- 
    TIMECAPS TimeCaps = {};
    timeGetDevCaps(&TimeCaps, sizeof(TimeCaps)); 
    // Set the clock resolution as high as possible for this system.
    timeBeginPeriod(TimeCaps.wPeriodMin); 
    
    // Global variables for the game loop.
    MSG msg = {}; //< Holds windows messages.
    game_input GameInput = {};
    win32_joystick_states JoystickStates = {};
    // Holds the starting time of a frame.
    LARGE_INTEGER StartingTime = {}; 
    LARGE_INTEGER EndingTime = {};
    // The target number of frames per second.
    uint16 GameUpdateFrequency = 60;
    // Target frame time in  miliseconds.
    real64 TargetFrameTime = (1000.0f / (real64)GameUpdateFrequency);  
    
    real64 ElapsedSeconds = 0;
    real64 ElapsedMiliseconds = 0;
    real64 ElapsedTimeSinceStartup = 0;
    // The amount of time there is left for waiting in this frame.
    real64 FrameTimeLeft = 0; 
    // Holds the amount of miliseconds that the frame was off the target frametime.
    real64 AverageMilisecondsOffTarget = 0;
    uint16 FPS = 0;
    
    bool32 IsFirstLoop = true;

HMODULE GameCode = {};

// The name of the game DLL that is currently being used.
char CurrentDLLName[MAX_PATH] = {};

//----------- GAME LOOP BEGIN -------------------
QueryPerformanceCounter(&StartingTime);
while(WindowInfo.IsRunning) 
    {
        ElapsedMiliseconds = Win32GetTimeDifference(StartingTime);
            FrameTimeLeft = TargetFrameTime - ElapsedMiliseconds;
        #if 0
        char textbuffer[256];
        //Format and print various data:   
        sprintf_s(textbuffer, 256 ,"Frametimeleft: %f \n", FrameTimeLeft);  
        OutputDebugStringA(textbuffer);
        #endif
            if(ElapsedMiliseconds < TargetFrameTime)
            {
                // This is the frame time +/- the time that we spend too long on the last frames accumulated
                // because of an unexpected hickup or an accumulation of small times spend oversleeping. 
                real64 AdjustedTargetFrameTime = TargetFrameTime - AverageMilisecondsOffTarget;
                
                // Spin lock until we reach the (adjusted) target frame time.
                while(ElapsedMiliseconds < AdjustedTargetFrameTime)
{
    // Save the ending time; this is the start time of the new frame.
    ElapsedMiliseconds = Win32GetTimeDifference(StartingTime, EndingTime);
            }
            }
            else
            {
                // We took too long to compute the frame and went over the target frame time.
                OutputDebugStringA("Went over the target frame time! \n");
                // Save the ending time; this is the start time of the new frame.
                ElapsedMiliseconds = Win32GetTimeDifference(StartingTime, EndingTime);
            }
            StartingTime = EndingTime;
            
            //GameSoundBuffer.SampleCount = (Win32GetSoundSamplesToWrite(WinSoundOutput, SecondarySoundBuffer, GameUpdateFrequency, GameBitmap, PlayWriteCursors));
            
            // Always write one frame of samples.
            GameSoundBuffer.SampleCount = (WinSoundOutput.SamplesPerSecond/GameUpdateFrequency);
            
            // Using the target frame time to calculate if we slept for too long and 
            // adding it to an accumulator. In the case that we're trying to make up for
            // a frame that took too long, this value will be negative. 
            // This means that 'AverageMilisecondsOffTarget' should always fluctuate around 0.
            AverageMilisecondsOffTarget += (ElapsedMiliseconds - TargetFrameTime);
            
            // Calculate how many complete frames we're behind so we can decrement.
            // The only thing we care about is the offset from any frame boundary.
            int16 nFramesInAverageMilisecondsOffTargets = (int16)(AverageMilisecondsOffTarget / TargetFrameTime);
            
            // Decrement the 'off target' frame time with the amount of whole frames.
            AverageMilisecondsOffTarget -= (nFramesInAverageMilisecondsOffTargets * TargetFrameTime);
            
ElapsedSeconds = (ElapsedMiliseconds / 1000.0f);
        FPS = (uint16)((1.0f / (real32)ElapsedSeconds) + 1); // + 1 to round to top value.
        ElapsedTimeSinceStartup += ElapsedSeconds;
        
             //-----------------------------------------------------------------------
             // Start of new frame. 
            
            
            char NewestDLLName[MAX_PATH] = {};
            Win32GetLatestGameDLLName(NewestDLLName);
            
            if(!(CompareCString(CurrentDLLName, NewestDLLName)))
            {
                // Make sure that we are getting something back.
                // Otherwise, use an old DLL.
                HMODULE GameCodeTemp = LoadLibraryA(NewestDLLName);
                
                if(GameCodeTemp)
                {
                    // Unlock the old DLL so it can be removed from memory.
                    FreeLibrary(GameCode);
                    GameCode = GameCodeTemp; 
                    CopyCString(NewestDLLName, CurrentDLLName);
                    
                    // Casting to a void* seems to be necessary because otherwise we get 
                    // warnings about unsafe typecasts from FARPROC to GAMEUPDATEPTR.
                    void* GameUpdateProcAddress = GetProcAddress(GameCode, "Update");
                    GameUpdatePtr = (GAMEUPDATEPTR)GameUpdateProcAddress;
                }
            }
            
#if OPENGL
            glClear(GL_COLOR_BUFFER_BIT);
            #endif
            IsFirstLoop = false;
        
        // Copy the latest state to previous state.
        memcpy_s(JoystickStates.PreviousState, sizeof(JoystickStates.PreviousState),
                 JoystickStates.NewState,  sizeof(JoystickStates.NewState));   
        
        *JoystickStates.NewState = {};
        
        Win32ProcessKeyboardInput(JoystickStates);
        Win32ProcessJoyStickInput(JoystickStates);
        
        // Copy the new state to the game input.
        memcpy_s(GameInput.Joysticks, sizeof(GameInput.Joysticks),
                 JoystickStates.NewState,  sizeof(JoystickStates.NewState));   
        
        GameInput.DeltaTime = ElapsedMiliseconds;
        GameInput.GameUpdateHz = GameUpdateFrequency;
        GameInput.PlatformLoadFile = Win32LoadFile;
        GameInput.PlatformGetFileSize = Win32GetFileSize;
        
        // Call the game when we have function address to the game.
        if(GameUpdatePtr)
        {
    GameUpdatePtr(GameInput, GameMemory, GameSoundBuffer, GameBitmap);
        }
        
        // Copy the game output buffer into directsound memory. 
        Win32CopySoundBuffer(AudioRenderClient, &GameSoundBuffer);
        
        uint32 OneFrameSampleCount = (WinSoundOutput.SamplesPerSecond/GameUpdateFrequency);
#if 0
        Win32DebugDrawFrameBoundaries(GameBitmap, OneFrameSampleCount, WinSoundOutput.SamplesPerSecond);
        
        Win32DebugDrawWriteCursor(GameBitmap, PlayWriteCursors, WinSoundOutput.SamplesPerSecond * WinSoundOutput.BytesPerSample);
        
        Win32DebugDrawWaveForm(GameBitmap, PlayWriteCursors, WinSoundOutput, GameUpdateFrequency, GameSoundBuffer);
        #endif
        
        #if OPENGL
        glDrawPixels(GameBitmap.Width, GameBitmap.Height, GL_BGRA_EXT, GL_UNSIGNED_BYTE, (GLubyte*)GameBitmap.BitmapMemory);
        SwapBuffers(DeviceContext);
        
#elif SOFTWARE_RENDERING
        Win32RenderBitmapToScreen(GameBitmap, WindowInfo.DeviceContext, WindowInfo.WindowHandle);
        #endif
        LARGE_INTEGER WindowProcessingTime = {};
        QueryPerformanceCounter(&WindowProcessingTime);
        }
//------------ GAME LOOP END -------------
        timeEndPeriod(TimeCaps.wPeriodMin); 

    return (int) msg.wParam;
}
    

