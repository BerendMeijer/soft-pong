#if GAME_DEBUG
#include <Windows.h>
#endif

#include "game_main.h"
#include "shared_helpers.cpp"

#include "game_memory.cpp"
#include "game_fileio.cpp"
#include "game_graphics.cpp"
#include "game_math.cpp"
#include "game_audio.cpp"
#include "game_world.cpp"
#include "game_algorithm.cpp"
#include "game_collision.cpp"

// Avoids name mangling with 'Update' that we want to export.
#ifdef __cplusplus
extern "C" {
#endif
    // This is the main (and only) entry point where to platform calls the game! 
    // All updating of the game happens from here out.
 __declspec(dllexport) 
        void Update(IN const game_input &a_Input, 
                IN OUT game_memory& a_Memory, 
                OUT game_sound_out& a_SoundBuffer, 
                OUT game_bitmap* a_Bitmap)
{
    game_world_objects* World = 0;
    
    // Set the pointer to transient memory so we can allocate memory.
    SetMemoryPointer(a_Memory.TransientMemory, a_Memory.TransientMemorySize);
    
    // Check if the game already is initialized. 
    // In that case the game was hot-reloaded, and we don't need to initialize.
    game_memory_state* MemoryState = (game_memory_state*)a_Memory.TransientMemory;
    if(!MemoryState->IsInitialized)
    {
        game_memory_state* Memory = (game_memory_state*)PushObject(sizeof(game_memory_state));
        MemoryState->IsInitialized = true;
        
        World = (game_world_objects*)PushObject(sizeof(game_world_objects));
        
        //------- Allocate memory for all gameobjects ---------
          World->PlayerOne = (game_object*)PushObject(sizeof(game_object));
        World->GameObjects.push_back(World->PlayerOne);
        
        World->PlayerTwo = (game_object*)PushObject(sizeof(game_object));
        World->GameObjects.push_back(World->PlayerTwo);
        
        World->PongBall = (game_object*)PushObject(sizeof(game_object));
        World->GameObjects.push_back(World->PongBall);
        
        World->TopLevelBounds = (game_object*)PushObject(sizeof(game_object));
        World->GameObjects.push_back(World->TopLevelBounds);
        
        World->BottomLevelBounds = (game_object*)PushObject(sizeof(game_object));
        World->GameObjects.push_back(World->BottomLevelBounds);
        
        World->TopTriangle = (game_object*)PushObject(sizeof(game_object));
        World->GameObjects.push_back(World->TopTriangle);
        
        World->BottomTriangle = (game_object*)PushObject(sizeof(game_object));
        World->GameObjects.push_back(World->BottomTriangle);
        
        
        World->BackgroundImageFile = (game_file*)PushObject(sizeof(game_file));
        
        World->BackgroundImageFile->FileSize = a_Input.PlatformGetFileSize("BackgroundGreen.bmp");
        World->BackgroundImageFile->FileMemory = PushObject(World->BackgroundImageFile->FileSize);
        a_Input.PlatformLoadFile("BackgroundGreen.bmp", true, World->BackgroundImageFile->FileSize, 
                                 World->BackgroundImageFile->FileMemory, 
                                 World->BackgroundImageFile->ImageData); 
        
        // Allocate memory for the file info.
        World->LeftPadImageFile = (game_file*)PushObject(sizeof(game_file));
            
        // Allocate memory for the file memory.
        World->LeftPadImageFile->FileSize = a_Input.PlatformGetFileSize("PadWood.bmp");
        World->LeftPadImageFile->FileMemory = PushObject(World->LeftPadImageFile->FileSize);
        
        // Load the file into memory.
        a_Input.PlatformLoadFile("PadWood.bmp", true, World->LeftPadImageFile->FileSize, 
                                 World->LeftPadImageFile->FileMemory, 
                                 World->LeftPadImageFile->ImageData); 
        
        
        //------------ Bottom triangle texture ------------ 
        // Allocate memory for the file info.
        World->BottomTriangleImageFile = (game_file*)PushObject(sizeof(game_file));
        
        // Allocate memory for the file memory.
        World->BottomTriangleImageFile->FileSize = a_Input.PlatformGetFileSize("BottomTriangleBlue.bmp");
        World->BottomTriangleImageFile->FileMemory = PushObject(World->BottomTriangleImageFile->FileSize);
        
        // Load the file into memory.
        a_Input.PlatformLoadFile("BottomTriangleBlue.bmp", true, World->BottomTriangleImageFile->FileSize, 
                                 World->BottomTriangleImageFile->FileMemory, 
                                 World->BottomTriangleImageFile->ImageData); 
        
        //----------- Top triangle texture -----------------
        // Allocate memory for the file info.
        World->TopTriangleImageFile = (game_file*)PushObject(sizeof(game_file));
        
        // Allocate memory for the file memory.
        World->TopTriangleImageFile->FileSize = a_Input.PlatformGetFileSize("TopTriangleBlue.bmp");
        World->TopTriangleImageFile->FileMemory = PushObject(World->TopTriangleImageFile->FileSize);
        
        // Load the file into memory.
        a_Input.PlatformLoadFile("TopTriangleBlue.bmp", true, World->TopTriangleImageFile->FileSize, 
                                 World->TopTriangleImageFile->FileMemory, 
                                 World->TopTriangleImageFile->ImageData); 
        
        
        //------------  Pongball texture ------------ 
        // Allocate memory for the file info.
        World->PongballImageFile = (game_file*)PushObject(sizeof(game_file));
        
        // Allocate memory for the file memory.
        World->PongballImageFile->FileSize = a_Input.PlatformGetFileSize("Ball.bmp");
        World->PongballImageFile->FileMemory = PushObject(World->PongballImageFile->FileSize);
        
        // Load the file into memory.
        a_Input.PlatformLoadFile("Ball.bmp", true, World->PongballImageFile->FileSize, 
                                 World->PongballImageFile->FileMemory, 
                                 World->PongballImageFile->ImageData); 
        
        InitializeWorld(*World);
    }
        
    // TODO(Berend): Allow objects to request a new memory arena.
    // This is only here so hot reloading still works. 
    // Getting a pointer somewhere because you know the memory layout, is terrible. 
    // And we should not depend on a fixed layout for sure.
    World = (game_world_objects*)(((uint8*)a_Memory.TransientMemory) + sizeof(game_memory_state));
    
    UpdateSynth(a_SoundBuffer);
    
    ClearScreen(a_Bitmap);
    
    //---------- Render background image ---------
    DrawBitmap(World->BackgroundImageFile->ImageData, {0.0f,0.0f}, *a_Bitmap);
    
    UpdatePlayerVelocityOnInput(*GetComponent<game_rigidbody2D>(*World->PlayerOne), 
                          (real32)(a_Input.Joysticks[0].Buttons.AButton.IsDown - a_Input.Joysticks[0].Buttons.BButton.IsDown),
                         (real32)a_Input.Joysticks[0].Buttons.BButton.IsDown,
                         (real32)a_Input.DeltaTime);
    
    UpdatePlayerVelocityOnInput(*GetComponent<game_rigidbody2D>(*World->PlayerTwo), 
                          (real32)(a_Input.Joysticks[0].Buttons.PadUp.IsDown - a_Input.Joysticks[0].Buttons.PadDown.IsDown),
                         (real32)a_Input.Joysticks[1].RightStickX.Value,
                         (real32)a_Input.DeltaTime);
    
    UpdateObjectPosition(*World->PlayerOne, (real32)a_Input.DeltaTime);
    UpdateObjectPosition(*World->PlayerTwo, (real32)a_Input.DeltaTime);
    UpdateObjectPosition(*World->PongBall, (real32)a_Input.DeltaTime);
    
    UpdateCollider(World->PongBall);
    UpdateCollider(World->PlayerOne);
    UpdateCollider(World->PlayerTwo);

    ResolveCollisions(World->GameObjects);
    
    ResetPongballOnOutOfBounds(World->PongBall);
    
    //--------- Draw colliders ----------------
    
    
    game_convex_collider2D* PlayerOneConvex = GetComponent<game_convex_collider2D>(*World->PlayerOne); 
    //DrawRect(*a_Bitmap, PlayerOneConvex->VertexList.Vertices[3], PlayerOneConvex->VertexList.Vertices[1], World->PlayerOne->Color);
    
    game_convex_collider2D* PlayerTwoConvex = GetComponent<game_convex_collider2D>(*World->PlayerTwo); 
    //DrawRect(*a_Bitmap, PlayerTwoConvex->VertexList.Vertices[3], PlayerTwoConvex->VertexList.Vertices[1], World->PlayerTwo->Color);
    
    game_convex_collider2D* PongBallConvex = GetComponent<game_convex_collider2D>(*World->PongBall); 
    //DrawRect(*a_Bitmap, PongBallConvex->VertexList.Vertices[3], PongBallConvex->VertexList.Vertices[1], {50, 50, 200});
    
    game_convex_collider2D* TopTriangleConvex  = GetComponent<game_convex_collider2D>(*World->TopTriangle); 
    //DrawTriangle(*a_Bitmap, TopTriangleConvex->VertexList.Vertices[0], 
                 //TopTriangleConvex->VertexList.Vertices[1], TopTriangleConvex->VertexList.Vertices[2], {0,20,198});
                 
                 game_convex_collider2D* BottomTriangleConvex  = GetComponent<game_convex_collider2D>(*World->BottomTriangle); 
                 
                 //DrawTriangle(*a_Bitmap, BottomTriangleConvex->VertexList.Vertices[0], 
                              //BottomTriangleConvex->VertexList.Vertices[1], BottomTriangleConvex->VertexList.Vertices[2], {0,20,198});
                              
                              DrawBitmap(World->LeftPadImageFile->ImageData, PlayerOneConvex->VertexList.Vertices[3], *a_Bitmap);
                              DrawBitmap(World->LeftPadImageFile->ImageData, PlayerTwoConvex->VertexList.Vertices[3], *a_Bitmap);
                              DrawBitmap(World->BottomTriangleImageFile->ImageData, BottomTriangleConvex->VertexList.Vertices[0], *a_Bitmap);
                              DrawBitmap(World->TopTriangleImageFile->ImageData, {TopTriangleConvex->VertexList.Vertices[0].X, TopTriangleConvex->VertexList.Vertices[2].Y} , *a_Bitmap);
                              DrawBitmap(World->PongballImageFile->ImageData, PongBallConvex->VertexList.Vertices[3], *a_Bitmap);
                              }
#ifdef __cplusplus
}
#endif