struct game_memory
{
    uint32 PermanentMemorySize; //< In bytes.
    uint32 TransientMemorySize; //< In bytes.
    void* PermanentMemory; //< Memory we will save to the drive at exit.
    void* TransientMemory; //< Memory for temporary use.
};

struct game_memory_state
{
    bool32 IsInitialized;
    uint32 ByteCount;
};