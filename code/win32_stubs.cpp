// These are stubs in case there is no XInput DLL on the computer we 
// are running on. In that case we can still run the program.
// We are using the original XInputGetState/XInputSetState function signature here
// to create a function pointer of that type that points to our lambda.
internal decltype(XInputGetState)* XInputGetState_= 
[](DWORD dwUserIndex, XINPUT_STATE* pState) ->DWORD
{
    return ERROR_DEVICE_NOT_CONNECTED;
};
// Same as the above!
internal decltype(XInputSetState)* XInputSetState_= 
[](DWORD dwUserIndex, XINPUT_VIBRATION* pVibration) ->DWORD
{
    return ERROR_DEVICE_NOT_CONNECTED;
};
// Stub for DirectSound.
internal decltype(DirectSoundCreate)* DirectSoundCreate_ = 
[](LPCGUID pcGuidDevice, LPDIRECTSOUND *ppDS, LPUNKNOWN pUnkOuter)
{
    return DSERR_NODRIVER;
};


// Make sure that anyone calling XInputGetState/XInputSetState will call the 
// stub by default. If the DLL is loaded, we make sure it points
// to the function in the DLL again.
#define XInputGetState XInputGetState_
#define XInputSetState XInputSetState_
#define DirectSoundCreate DirectSoundCreate_