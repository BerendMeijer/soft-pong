/// Overwrites the entire bitmap with a solid color. 
internal void 
ClearScreen(OUT game_bitmap* a_Bitmap)
{
    // Point to the first pixel.
    uint32* Pixel = (uint32*)a_Bitmap->Memory; 
    
    uint8* PixelColour = (uint8*)Pixel;
    
    for(uint16 Yiterator = 0; Yiterator < a_Bitmap->Height; ++Yiterator)
    {
        for(uint16 Xiterator = 0; Xiterator < a_Bitmap->Width; ++Xiterator) 
        {
            // Orange.
            *PixelColour++ = 50; //Blue
            *PixelColour++ = 161; //Green
            *PixelColour++ = 255; //Red
            PixelColour++; // Unused.
        }
        // Point to the next row.
        Pixel += a_Bitmap->Width;
    }
}

/// Clips a 2D point to screen space, and returns
/// the resulting point.
internal game_vec2
     Clip2DPointToSceen(IN game_vec2 a_Point)
 {
     game_vec2 Result = {};
     
     real32 MinYScreenCoordinate = 0.0f;
     real32 MaxYScreenCoordinate = 1.0f;
     real32 MinXScreenCoordinate = 0.0f;
     real32 MaxXScreenCoordinate = 1.0f;
     
     //====== X =========
     if(a_Point.X < MinXScreenCoordinate)
     {
         Result.X = MinXScreenCoordinate;
     }
     else if(a_Point.X > MaxXScreenCoordinate)
     {
         Result.X = MaxXScreenCoordinate;
     }
     else
     {
         // If the above wasnt true we dont have to clip.
         Result.X = a_Point.X;
     }
     
     //======= Y =========
     if(a_Point.Y < MinYScreenCoordinate)
     {
         Result.Y = MinYScreenCoordinate;
     }
     else if(a_Point.Y > MaxYScreenCoordinate)
     {
         Result.Y = MaxYScreenCoordinate;
     }
     else
     {
         // If the above wasnt true we dont have to clip.
         Result.Y = a_Point.Y;
     }
     
     return(Result);
 }
 
 /// Draws a bitmap onto the screenbuffer at a given screen position.
 /// The position is used as the left bottom anchor of the bitmap.
 internal void
     DrawBitmap(IN const game_bitmap& a_BitmapToDraw,
                IN game_vec2 a_ScreenPosition, 
                OUT game_bitmap& a_ScreenBuffer)
 {
     // Making a hard assumption here that there will be our screen limits.
     game_vec2 MinScreenLimit = {0.0f, 0.0f};
     game_vec2 MaxScreenLimit = {1.0f, 1.0f};
     
     //------ Clip left and bottom ------
     // Get the clipping values to left and top of bottom of the bitmap that we are drawing on.
     real32 XOffscreenLeft = a_ScreenPosition.X < MinScreenLimit.X ? (a_ScreenPosition.X * -1.0f) : MinScreenLimit.X;
     real32 YOffscreenBottom = a_ScreenPosition.Y < MinScreenLimit.Y ? (a_ScreenPosition.Y * -1.0f) : MinScreenLimit.Y;
     // Calculate how many pixels we are offscreen, and round to the upper value with ceil().
     // If a pixel is 3/4th part offscreen we cannot draw it.
     uint32 XPixelsOffscreenLeft = (uint32)(ceil(XOffscreenLeft * a_ScreenBuffer.Width));
     uint32 YPixelsOffscreenBottom = (uint32)(ceil(YOffscreenBottom * a_ScreenBuffer.Height));
     
     // Set the start of bitmaptodraw memory to this offset, this is the pixel offset that we can start
     // drawing from.
     uint32 BitmapToDrawOffset = XPixelsOffscreenLeft + (YPixelsOffscreenBottom * a_BitmapToDraw.Width);
     
     // Clip the desired drawing position to screen space so we can calculate a valid starting point.
     a_ScreenPosition = Clip2DPointToSceen(a_ScreenPosition);
     
     // Transform the world space coordinates to pixel coordinates 
     // to determine where to start drawing the bitmap.
     uint32 XOffset = (uint32)(a_ScreenPosition.X * (real32)a_ScreenBuffer.Width);
     uint32 YOffset = (uint32)(a_ScreenPosition.Y * (real32)a_ScreenBuffer.Height);
     uint32 TotalOffset = XOffset + (YOffset * a_ScreenBuffer.Width); 
     
     // ------ Clip right and top ---------
     // What if the bitmap we're drawing on is offscreen??
     // We only want to draw on top of the part that is onscreen.
     
     // How many pixels do we have from the offset to the end of the bitmap we're drawing onto?
     uint32 PixelsToEndX = a_ScreenBuffer.Width - XOffset;
     // Determine if we can draw all pixels of a_BitmapToDraw, or a portion.
     int32 PixelsToDrawX = (int32)((PixelsToEndX > a_BitmapToDraw.Width) ? a_BitmapToDraw.Width : PixelsToEndX);
     // Decrement the width by the amount of pixels offscreen on the left, if any.
     PixelsToDrawX-= XPixelsOffscreenLeft;
     
     // Clip the top of the bitmap we're drawing on.
     uint32 PixelsToEndY = a_ScreenBuffer.Height - YOffset;
     int32 PixelsToDrawY  = (int32)((PixelsToEndY > a_BitmapToDraw.Height) ? a_BitmapToDraw.Height : PixelsToEndY);
     PixelsToDrawY-= YPixelsOffscreenBottom;
     
     // Point to the first pixelrow to start drawing onto.
     uint32* DestPixelRow = ((uint32*)a_ScreenBuffer.Memory) + TotalOffset; 
     uint32* SourcePixelRow = ((uint32*)a_BitmapToDraw.Memory) + BitmapToDrawOffset;
     
     for(uint16 Yiterator = 0; Yiterator < PixelsToDrawY; ++Yiterator)
     {
         for(uint16 Xiterator = 0; Xiterator < PixelsToDrawX; ++Xiterator) 
         {
             // Get the alpha value for this pixel.
             uint8 AlphaValue = *((uint8*)(SourcePixelRow + Xiterator) + 3);
             if(AlphaValue)
             {
                 uint32* CurrentDestPixel = (DestPixelRow + Xiterator); 
                 uint32* CurrentSourcePixel = (SourcePixelRow + Xiterator);
                 
                 // Point to the colours in the pixel.
                 uint8* CurrentDestColour = (uint8*)CurrentDestPixel;
                 uint8* CurrentSourceColour = (uint8*)CurrentSourcePixel;
                 
                 real32 AlphaScalar = (AlphaValue / 255.0f);
                 
                 // Alpha blending.
                 *(CurrentDestColour) = (uint8)((*CurrentSourceColour * AlphaScalar) + (*CurrentDestColour * (1 - AlphaScalar)));
                 *(CurrentDestColour + 1) = (uint8)((*(CurrentSourceColour + 1) * AlphaScalar) + (*(CurrentDestColour + 1) * (1 - AlphaScalar)));
                 *(CurrentDestColour + 2) = (uint8)((*(CurrentSourceColour + 2) * AlphaScalar) + (*(CurrentDestColour + 2) * (1 - AlphaScalar)));
             }
         }
         // Point to the next row of pixels.
         DestPixelRow += a_ScreenBuffer.Width;
         SourcePixelRow += a_BitmapToDraw.Width;
     }
 }
 
 
 /// Draws a solid rectangle on a the supplied bitmap, in the color
 /// a_Color, between the two worldspace coordinates supplied.
 internal void
     DrawRect(OUT game_bitmap& a_Bitmap, 
              IN game_vec2 a_LeftBottomCorner, 
              IN game_vec2 a_RightTopCorner,
              IN const game_color& a_Color)
 {
     // Clip the rectangle to the screen.
     a_LeftBottomCorner = Clip2DPointToSceen(a_LeftBottomCorner);  
     a_RightTopCorner = Clip2DPointToSceen(a_RightTopCorner);
     
     // Calculate the left top corner pixel we need to point to to start filling 
     // a rectangle with color.
     uint32 XOffset = (uint32)(a_LeftBottomCorner.X * (real32)a_Bitmap.Width);
     uint32 YOffset = (uint32)(a_LeftBottomCorner.Y * (real32)a_Bitmap.Height);
     uint32 TotalOffset = XOffset + (YOffset * a_Bitmap.Width); 
     
     // Point to the first pixel (left top corner of the rectangle).
     uint32* Pixel = (uint32*)a_Bitmap.Memory + TotalOffset;
     
     uint8* PixelColour = (uint8*)Pixel;
     
     // The function in specified to draw from a pixel that the first given coordinate falls 
     // to  but not including the pixel that the second coordinate falls into.
     int32 RectHeight = (int32)((a_RightTopCorner.Y - a_LeftBottomCorner.Y) * (real32)a_Bitmap.Height);
     int32 RectWidth = (int32)((a_RightTopCorner.X - a_LeftBottomCorner.X) * (real32)a_Bitmap.Width);
     
     for(int32 Y = 0; Y < RectHeight; ++Y)
     {
         PixelColour = (uint8*)Pixel;
         
         for(int32 X = 0; X < RectWidth; ++X)
         {
             *PixelColour++ = a_Color.Blue;
             *PixelColour++ = a_Color.Green;
             *PixelColour++ = a_Color.Red;
             PixelColour++;
         }
         Pixel += (a_Bitmap.Width);
     }
 }
 
 
 /// @a_Size Default size is 1.
 internal void 
     DrawPoint(OUT game_bitmap& a_Bitmap, 
               IN game_vec2 a_Position,
               IN game_color a_Color,
               IN real32 a_Size)
 {
     game_vec2 LeftBottomCorner = {};
     game_vec2 RightTopCorner = {};
     
     RightTopCorner.X = a_Position.X + (0.01f * a_Size);
     RightTopCorner.Y = a_Position.Y + (0.01f * a_Size);
     
     LeftBottomCorner.X = a_Position.X - (0.01f * a_Size);
     LeftBottomCorner.Y = a_Position.Y - (0.01f * a_Size);
     
     DrawRect(a_Bitmap, LeftBottomCorner, RightTopCorner, a_Color);
 }
 
 
 internal void 
     DrawLine(OUT game_bitmap& a_Bitmap, 
              IN game_vec2 a_PointA,
              IN game_vec2 a_PointB, 
              IN game_color a_Color)
 {
     a_PointA = Clip2DPointToSceen(a_PointA);
     a_PointB = Clip2DPointToSceen(a_PointB);
     
     game_vec2 StartPoint = {};
     game_vec2 EndPoint = {};
     
     // Choose the X axis to iterate over if it is longer, else iterate over the Y axis. 
     // This ensures that there will never be a gap in the line.
     real32 XLength = a_PointA.X - a_PointB.X;
     real32 YLength = a_PointA.Y - a_PointB.Y;
     // Take the absolute if an axis has a negative length.
     if(fabs(XLength) > fabs(YLength))
     {
         // Set the leftmost point as the starting point.
         // We'll iterate over the pixels in width from left to right.
         if(a_PointA.X < a_PointB.X)
         {
             StartPoint = a_PointA;
             EndPoint = a_PointB;
         }
         else
         {
             StartPoint = a_PointB;
             EndPoint = a_PointA;
         }
         
         XLength = EndPoint.X - StartPoint.X;
         YLength = EndPoint.Y - StartPoint.Y;
         
         // The acual length of a pixel in our coordinate system.
         // In our coordinate system, 1.0f is the max value for the screen.
         real32 PixelLength = 1.0f / (real32)a_Bitmap.Width;
         
         // The amount of pixels that fit in the length of the line that fit on the X axis.
         int16 LinePixelLengthX = (int16)(XLength / PixelLength);
         
         // How we need to increment or decrement per pixel in the X direction.
         real32 YStepPerX =  YLength /(real32)LinePixelLengthX; 
         
         for(int16 PixelIndex = 0; PixelIndex < LinePixelLengthX; ++PixelIndex)
         {
             // Transform the world space coordinates to pixel coordinates.
             uint32 XOffset = (uint32)(StartPoint.X * (real32)a_Bitmap.Width);
             uint32 YOffset = (uint32)(StartPoint.Y * (real32)a_Bitmap.Height);
             uint32 TotalOffset = XOffset + (YOffset * a_Bitmap.Width); 
             
             // Point to the first pixel.
             uint32* Pixel = (uint32*)a_Bitmap.Memory + TotalOffset;
             uint8* PixelColour = (uint8*)Pixel;
             
             *PixelColour++ = a_Color.Blue;
             *PixelColour++ = a_Color.Green;
             *PixelColour++ = a_Color.Red;
             PixelColour++;
             
             // Increment the startingpoint in the direction of the endpoint.
             StartPoint.X += PixelLength;
             StartPoint.Y += YStepPerX;
         }
     } 
     // We are going to iterate over the line on the Y axis because it 
     // has a larger length than the line length on the X axis.
     else 
     {
         // Pick the lowest point on the Y axis.
         if(a_PointA.Y < a_PointB.Y)
         {
             StartPoint = a_PointA;
             EndPoint = a_PointB;
         }
         else
         {
             StartPoint = a_PointB;
             EndPoint = a_PointA;
         }
         
         YLength = EndPoint.Y - StartPoint.Y;
         XLength = EndPoint.X - StartPoint.X;
         
         // The acual length of a pixel in our coordinate system.
         // In our coordinate system, 1.0f is the max value for the screen.
         real32 PixelHeight = 1.0f / (real32)a_Bitmap.Height;
         
         // The amount of pixels that fit in the length of the line on the Y axis.
         int16 LinePixelLengthY = (int16)(YLength / PixelHeight);
         real32 XStepPerY = XLength /(real32)LinePixelLengthY;
         for(int16 PixelIndex = 0; PixelIndex < LinePixelLengthY; ++PixelIndex)
         {
             // Transform the world space coordinates to pixel coordinates.
             uint32 XOffset = (uint32)(StartPoint.X * (real32)a_Bitmap.Width);
             uint32 YOffset = (uint32)(StartPoint.Y * (real32)a_Bitmap.Height);
             uint32 TotalOffset = XOffset + (YOffset * a_Bitmap.Width); 
             
             // Point to the pixel.
             uint32* Pixel = (uint32*)a_Bitmap.Memory + TotalOffset;
             uint8* PixelColour = (uint8*)Pixel;
             
             *PixelColour++ = a_Color.Blue;
             *PixelColour++ = a_Color.Green;
             *PixelColour++ = a_Color.Red;
             PixelColour++;
             
             // Increment the startingpoint in the direction of the endpoint.
             StartPoint.Y += PixelHeight;
             StartPoint.X += XStepPerY;
         }
     }
 }
 
 /// Draws lines between three worldspace points.
 internal void
     DrawTriangle(OUT game_bitmap& a_Bitmap, 
                  IN game_vec2 a_X,
                  IN game_vec2 a_Y,
                  IN game_vec2 a_Z,
                  IN const game_color& a_Color)
 {
     DrawLine(a_Bitmap, a_X, a_Y, a_Color);
     DrawLine(a_Bitmap, a_X, a_Z, a_Color);
     DrawLine(a_Bitmap, a_Y, a_Z, a_Color);
 }
 