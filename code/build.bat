@echo off
IF NOT EXIST ..\..\build  mkdir  ..\..\build
pushd ..\..\build 

REM Clean up any unused old game files. If the files are used, access should be denied. 


REM ========== Flag info ====================
REM -nologo : Dont show compiler version info. 
REM -Od : Disables optimizations. 
REM -fp:fast : Fast floating point math, at the expense of accuracy.
REM -FC : Show full code path for warnings and errors. 
REM -Zi : Generate full debugging information.
REM -EHa : Use the exception-handling model that catches both asynchronous (structured) and synchronous (C++) exceptions.

REM -Wall : Show all warnings. 
REM -WX : Treat warnings as errors.
REM -wd4100 : Dont show unreferenced local parameters.
REM -wd4820 : Dont show padding warnings.
REM -wd4505 : Unreferenced local function has been removed.
REM -wd4189 : Unreferenced local variable. 
REM -wd4668 : 'Symbol' is not defined as a preprocessor macro, replacing with '0' for 'directives'.
REM -wd4710 : Function not inlined (sprintf_s)

set Warningflags= -WX -Wall -wd4100 -wd4189 -wd4820 -wd4505 -wd4514 -wd4668 -wd4710

set BuildDefines= -DGAME_DEBUG=1 -DWINDOWS=1 -DSOFTWARE_RENDERING=1 -DOPENGL=0

REM -LD : Create a DLL. 
REM -Fe[name] : names the DLL.
REM -opt:ref : Remove any unreferenced functions.
REM -map : Creates a map with the name of the source.
REM -mapinfo:exports : Includes exported functions in the map.

REM Remove any old game files. 
DEL  game*.*

REM Build the game DLL.
cl -nologo -Od -fp:fast -FC -Zi -EHa  %Warningflags% %BuildDefines% ..\code\game_main.cpp  -LD -Fegame%RANDOM%  /link -opt:ref -map -mapinfo:exports 
REM Build the platform executable.
cl -nologo -Od -fp:fast -FC -Zi -EHa  %Warningflags% %BuildDefines%  ..\code\win32_main.cpp User32.lib Dsound.lib XInput.lib Gdi32.lib Winmm.lib opengl32.lib Glu32.lib Ole32.lib


popd