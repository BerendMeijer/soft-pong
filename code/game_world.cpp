     #include <typeinfo>
     
     /// Adds a component of type T to the list of components of the game object a_GameObject.
     /// T must be derived from game_component.
     /// Returns a pointer to the component.
     template <typename T>
         internal T* AddComponent(game_object& a_GameObject) 
     {
         T* ComponentMemory = (T*)PushObject(sizeof(T));
         
         // Checking the type of the component and saving it an an enum so we dont have
         // to rely on dynamic casting, and can speed things up for GetComponent<T> calls.
         if(typeid(T).name() == typeid(game_rigidbody2D).name())
         {
             ComponentMemory->Type = e_componenttype::Rigidbody2D;
         }
         else if(typeid(T).name() == typeid(game_convex_collider2D).name())
         {
             ComponentMemory->Type = e_componenttype::ConvexCollider2D;
         }
          
         a_GameObject.Components.push_back(ComponentMemory);
         return ComponentMemory;
     }
     
     /// Gets a component of type T to the list of components of the game object a_GameObject.
     /// Returns a pointer to the component, if it found one.
     /// If it didn't, it returns null.
     template<typename T>
         internal T* 
         GetComponent(IN game_object& a_GameObject) 
     {
         T* Result = 0;
         
         e_componenttype ComponentToLookFor =  e_componenttype::NullComponent;
         if(typeid(T).name() == typeid(game_rigidbody2D).name())
            {
                ComponentToLookFor = e_componenttype::Rigidbody2D;
            }
            else if(typeid(T).name() == typeid(game_convex_collider2D).name())
            {
                 ComponentToLookFor = e_componenttype::ConvexCollider2D;
            }
            
            // Search in the list of components for the gameobject.
            // If none was found, Result stays null.
         for(uint16 Index = 0; Index < a_GameObject.Components.size(); ++Index)
         {
             if(a_GameObject.Components[Index]->Type == ComponentToLookFor)
             {
                 Result = (T*)a_GameObject.Components[Index];
                 break;
             }
         }
         return(Result);
     }
     
     internal void
         ResetPongballOnOutOfBounds(IN game_object* a_PongBall)
     {
         // Reset the ball if it goes out of bounds.
         if(a_PongBall->Position.X > 1.0f || a_PongBall->Position.X < 0.0f)
     {
         a_PongBall->Position.X = 0.5f;
         a_PongBall->Position.Y = 0.5f;
         
         game_synth_envelope Env = {};
         PlayNote(e_waveform::SQUARE, GetKeyFrequency(52), 100.0, 0.0f, Env, 0.1f);
         PlayNote(e_waveform::SQUARE, GetKeyFrequency(59), 100.0, 0.0f, Env, 0.1f);
     }
 }
     
     
     /// Sets up all game objects position, and components.
     internal void
         InitializeWorld(IN game_world_objects& a_World)
     {
         //--------- Setup player one ------------
         a_World.PlayerOne->Position.X = 0.05f;
         a_World.PlayerOne->Position.Y = 0.50f;
         a_World.PlayerOne->Height = 0.20f;
         a_World.PlayerOne->Width = 0.05f;
         a_World.PlayerOne->Color = {155, 229, 100};
         
         
         game_rigidbody2D* PlayerOneRigidBody = AddComponent<game_rigidbody2D>(*a_World.PlayerOne);
         PlayerOneRigidBody->Velocity = {0.0f, 0.0f};
         PlayerOneRigidBody->Mass = 1000.0f;
         PlayerOneRigidBody->Drag = 0.05f;
         PlayerOneRigidBody->IsKinematic = false;
         
         
         game_convex_collider2D* PlayerOneConvex = AddComponent<game_convex_collider2D>(*a_World.PlayerOne);
         PlayerOneConvex->VertexList.VertexCount = 4;
         // Left top.
         PlayerOneConvex->VertexList.Offsets[0].X = -(0.5f * a_World.PlayerOne->Width);
         PlayerOneConvex->VertexList.Offsets[0].Y = (0.5f * a_World.PlayerOne->Height);
         // Right top.
         PlayerOneConvex->VertexList.Offsets[1].X = (0.5f * a_World.PlayerOne->Width);
         PlayerOneConvex->VertexList.Offsets[1].Y = (0.5f * a_World.PlayerOne->Height);
         // Right bottom.
         PlayerOneConvex->VertexList.Offsets[2].X = (0.5f * a_World.PlayerOne->Width);
         PlayerOneConvex->VertexList.Offsets[2].Y = -(0.5f * a_World.PlayerOne->Height);
         // Left bottom.
         PlayerOneConvex->VertexList.Offsets[3].X = -(0.5f * a_World.PlayerOne->Width);
         PlayerOneConvex->VertexList.Offsets[3].Y = -(0.5f * a_World.PlayerOne->Height);
         
         //-------- Setup player two ------------
         a_World.PlayerTwo->Position.X = 0.95f;
         a_World.PlayerTwo->Position.Y = 0.50f;
         a_World.PlayerTwo->Height = 0.20f;
         a_World.PlayerTwo->Width = 0.05f;
         a_World.PlayerTwo->Color = {215, 247, 91};
         
         game_rigidbody2D* PlayerTwoRigidBody = AddComponent<game_rigidbody2D>(*a_World.PlayerTwo);
         PlayerTwoRigidBody->Velocity = {0.0f, 0.0f};
         PlayerTwoRigidBody->Mass = 1000.0f;
         PlayerTwoRigidBody->Drag = 0.05f;
         
         game_convex_collider2D* PlayerTwoConvex = AddComponent<game_convex_collider2D>(*a_World.PlayerTwo);
         PlayerTwoConvex->VertexList.VertexCount = 4;
         // Left top.
         PlayerTwoConvex->VertexList.Offsets[0].X = -(0.5f * a_World.PlayerTwo->Width);
         PlayerTwoConvex->VertexList.Offsets[0].Y = (0.5f * a_World.PlayerTwo->Height);
         // Right top.
         PlayerTwoConvex->VertexList.Offsets[1].X = (0.5f * a_World.PlayerTwo->Width);
         PlayerTwoConvex->VertexList.Offsets[1].Y = (0.5f * a_World.PlayerTwo->Height);
         // Right bottom.
         PlayerTwoConvex->VertexList.Offsets[2].X = (0.5f * a_World.PlayerTwo->Width);
         PlayerTwoConvex->VertexList.Offsets[2].Y = -(0.5f * a_World.PlayerTwo->Height);
         // Left bottom.
         PlayerTwoConvex->VertexList.Offsets[3].X = -(0.5f * a_World.PlayerTwo->Width);
         PlayerTwoConvex->VertexList.Offsets[3].Y = -(0.5f * a_World.PlayerTwo->Height);
         
         //------- Setup pong ball ------------
         a_World.PongBall->Position.X = 0.50f;
         a_World.PongBall->Position.Y = 0.50f;
         a_World.PongBall->Height = 0.02f;
         a_World.PongBall->Width = 0.02f;
         a_World.PongBall->Color = {0, 173, 192};
         
         game_rigidbody2D* PongBallRigidBody = AddComponent<game_rigidbody2D>(*a_World.PongBall);
         PongBallRigidBody->Velocity = {6.0f, 1.0f};
         PongBallRigidBody->Mass = 1.0f;
         
         game_convex_collider2D* PongConvex = AddComponent<game_convex_collider2D>(*a_World.PongBall);
         PongConvex->VertexList.VertexCount = 4;
         // Left top.
         PongConvex->VertexList.Offsets[0].X =  -(0.5f * a_World.PongBall->Width);
         PongConvex->VertexList.Offsets[0].Y = (0.5f * a_World.PongBall->Height);
         // Right top.
         PongConvex->VertexList.Offsets[1].X = (0.5f * a_World.PongBall->Width);
         PongConvex->VertexList.Offsets[1].Y = (0.5f * a_World.PongBall->Height);
         // Right bottom.
         PongConvex->VertexList.Offsets[2].X = (0.5f * a_World.PongBall->Width);
         PongConvex->VertexList.Offsets[2].Y = -(0.5f * a_World.PongBall->Height);
         // Left bottom.
         PongConvex->VertexList.Offsets[3].X = -(0.5f * a_World.PongBall->Width);
         PongConvex->VertexList.Offsets[3].Y = -(0.5f * a_World.PongBall->Height);
         
         //------ Setup top triangle ---------
         game_rigidbody2D* TopTriangleRigidbody = AddComponent<game_rigidbody2D>(*a_World.TopTriangle);
         TopTriangleRigidbody->Mass = 0xFFFFFF; // Static object.
         TopTriangleRigidbody->IsKinematic = true;
         
         game_convex_collider2D* TopTriangleConvex = AddComponent<game_convex_collider2D>(*a_World.TopTriangle);
         TopTriangleConvex->VertexList.VertexCount = 3;
         // Left top
         TopTriangleConvex->VertexList.Vertices[0].X = 0.25f;
         TopTriangleConvex->VertexList.Vertices[0].Y = 1.0f;
         // Right top.
         TopTriangleConvex->VertexList.Vertices[1].X = 0.75f;
         TopTriangleConvex->VertexList.Vertices[1].Y = 1.0f;
         // Bottom.
         TopTriangleConvex->VertexList.Vertices[2].X = 0.5f;
         TopTriangleConvex->VertexList.Vertices[2].Y = 0.80f;
         
         //------- Setup bottom triangle --------
         game_rigidbody2D* BottomTriangleRigidbody = AddComponent<game_rigidbody2D>(*a_World.BottomTriangle);
         BottomTriangleRigidbody->Mass = 0xFFFFFF; // Static object.
         BottomTriangleRigidbody->IsKinematic = true;
         
         game_convex_collider2D* BottomTriangleConvex = AddComponent<game_convex_collider2D>(*a_World.BottomTriangle);
         BottomTriangleConvex->VertexList.VertexCount = 3;
         // Left top
         BottomTriangleConvex->VertexList.Vertices[0].X = 0.25f;
         BottomTriangleConvex->VertexList.Vertices[0].Y = 0.0f;
         // Right top.
         BottomTriangleConvex->VertexList.Vertices[1].X = 0.75f;
         BottomTriangleConvex->VertexList.Vertices[1].Y = 0.0f;
         // Bottom.
         BottomTriangleConvex->VertexList.Vertices[2].X = 0.5f;
         BottomTriangleConvex->VertexList.Vertices[2].Y = 0.20f;
         
         //------- Setup top level bounds  ---------
         game_rigidbody2D* TopLevelBoundsRigidbody = AddComponent<game_rigidbody2D>(*a_World.TopLevelBounds);
         TopLevelBoundsRigidbody->Mass = 0xFFFFFF; // Static object.
         TopLevelBoundsRigidbody->IsKinematic = true;
         
         
         game_convex_collider2D* TopLevelBoundsConvex = AddComponent<game_convex_collider2D>(*a_World.TopLevelBounds);
         TopLevelBoundsConvex->VertexList.VertexCount = 4;
         // Left top.
         TopLevelBoundsConvex->VertexList.Vertices[0].X = -0.5f;
         TopLevelBoundsConvex->VertexList.Vertices[0].Y = 0xFFFFFF;
         // Right top.
         TopLevelBoundsConvex->VertexList.Vertices[1].X = 1.5;
         TopLevelBoundsConvex->VertexList.Vertices[1].Y = 0xFFFFFF;
         // Right bottom.
         TopLevelBoundsConvex->VertexList.Vertices[2].X = 1.5f;
         TopLevelBoundsConvex->VertexList.Vertices[2].Y = 1.0f;
         // Left bottom.
         TopLevelBoundsConvex->VertexList.Vertices[3].X = -0.5f;
         TopLevelBoundsConvex->VertexList.Vertices[3].Y = 1.0f;
         
         //------- Setup bottom level bounds --------
         game_rigidbody2D* BottomLevelBoundsRigidbody = AddComponent<game_rigidbody2D>(*a_World.BottomLevelBounds);
         BottomLevelBoundsRigidbody->Mass = 0xFFFFFF; // Static object.
         BottomLevelBoundsRigidbody->IsKinematic = true;
         
         game_convex_collider2D* BottomLevelBoundsConvex = AddComponent<game_convex_collider2D>(*a_World.BottomLevelBounds);
         BottomLevelBoundsConvex->VertexList.VertexCount = 4;
         // Left top.
         BottomLevelBoundsConvex->VertexList.Vertices[0].X = -0.5f;
         BottomLevelBoundsConvex->VertexList.Vertices[0].Y = 0.0f;
         // Right top.
         BottomLevelBoundsConvex->VertexList.Vertices[1].X = 1.5;
         BottomLevelBoundsConvex->VertexList.Vertices[1].Y = 0.0f;
         // Right bottom.
         BottomLevelBoundsConvex->VertexList.Vertices[2].X = 1.5f;
         BottomLevelBoundsConvex->VertexList.Vertices[2].Y = -0xFFFFFF;
         // Left bottom.
         BottomLevelBoundsConvex->VertexList.Vertices[3].X = -0.5f;
         BottomLevelBoundsConvex->VertexList.Vertices[3].Y = -0xFFFFFF;
     }
     
     /// Adds velocity to the Y axis of the rigidbody, if there is input.
     /// Decreases the velocity with the drag value.
internal void
         UpdatePlayerVelocityOnInput(OUT game_rigidbody2D& a_Rigidbody, 
                              IN real32 a_ThumbStickYValue,
                              IN real32 a_ThumbStickXValue,
                              IN real32 a_DeltaTime)
     {
         // TODO(Berend): Don't hardcode this..
         real32 MovementSpeed = 0.05f;
         
         a_Rigidbody.Velocity.Y += a_ThumbStickYValue * MovementSpeed * (real32)a_DeltaTime;
         a_Rigidbody.Velocity.Y *= (1.0f - a_Rigidbody.Drag);
     }
     
     
     internal void
         UpdateObjectPosition(IN OUT game_object& a_Object, 
                              IN real32 a_DeltaTime)
     {
         // In our world, 1.0f is the total width and height of the screen space.
         // The world is 1000 units wide and high:
         real32 OneWorldUnitSize = 0.0001f;
         
         game_rigidbody2D* Rigidbody = GetComponent<game_rigidbody2D>(a_Object);
         
         real32 ObjectYMovement = Rigidbody->Velocity.Y * (OneWorldUnitSize * (real32)a_DeltaTime);
         real32 ObjectXMovement = Rigidbody->Velocity.X * (OneWorldUnitSize * (real32)a_DeltaTime);
         
         a_Object.Position.Y += ObjectYMovement;
         a_Object.Position.X += ObjectXMovement;
     }
     

     