/// Gets the normalized vector between to 2D points in space.
/// Pointing from a_VectorA to a_VectorB.
internal game_vec2
GetNormalizedAxis(IN game_vec2 a_VectorA,IN game_vec2 a_VectorB)
{
    game_vec2 Result = {};
    
    // Calculate the point relative axis vector from two point of convex A. 
    Result.X = a_VectorB.X - a_VectorA.X;
    Result.Y = a_VectorB.Y - a_VectorA.Y;
    
    // Calculate magnitude (vector length).
    real32 VectorLength = (real32)sqrt(pow(Result.X, 2) + pow(Result.Y, 2));
    
    // Normalize axis vector.
    Result.X /= VectorLength;
    Result.Y /= VectorLength;
    
    return Result;
}



/// Gets the dotproduct between two 2D vectors.
/// The order of the vectors does not matter.
internal real32
DotProduct(IN game_vec2 a_VectorA, IN game_vec2 a_VectorB)
{
    real32 Result = 0;
    
    Result = (a_VectorA.X * a_VectorB.X) + (a_VectorA.Y * a_VectorB.Y);
    
    return(Result);
}

/// Reflects a_VectorToReflect onto a_VectorToReflectOn,
/// and returns the result.
internal game_vec2
ReflectVector(IN game_vec2 a_VectorToReflect, 
              IN game_vec2 a_VectorToReflectOn)
{
    game_vec2 Result = {};
    
    real32 VectorDot = DotProduct(a_VectorToReflect, a_VectorToReflectOn);
    Result.X = -2*(VectorDot) * a_VectorToReflectOn.X + a_VectorToReflect.X; 
    Result.Y = -2*(VectorDot) * a_VectorToReflectOn.Y + a_VectorToReflect.Y; 
    
    return Result;
}

/// Gets the perpendicular 2D vector perpendicular to 
/// a_Vector pointing to the right from the vector's perspective.
internal game_vec2
GetPerpendicularVectorClockwise(IN game_vec2 a_Vector)
{
    game_vec2 Result = {};
    
    Result.X = a_Vector.Y;
    Result.Y = -a_Vector.X;
    
    return Result;
}

/// Gets the perpendicular 2D vector perpendicular to 
/// a_Vector pointing to the left from the vector's perspective.
internal game_vec2
GetPerpendicularVectorCounterClockwise(IN game_vec2 a_Vector)
{
    game_vec2 Result = {};
    
    Result.X = -a_Vector.Y;
    Result.Y = a_Vector.X;
    
    return Result;
}

