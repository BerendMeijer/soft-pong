#pragma once
#include "shared_defines.h"

    #define CONVEX_MAX_VERTEX_COUNT 256
    
#include "game_memory.h"
#include "game_audio.h"
     

#include <math.h>
#include "game_math.h"
#include "game_collision.h"
#include "game_graphics.h"
#include "game_input.h"
#include "game_fileio.h"
#include "game_world.h"
